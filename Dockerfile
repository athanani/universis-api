FROM node:14-alpine AS builder
# set working directory
WORKDIR /app
# copy project
COPY . .
# run pre-installation tasks
RUN npm run preinstall
# install dependencies
RUN npm ci
# build 
RUN npm run build
# remove dependencies
RUN rm -rf node_modules

FROM node:14-alpine AS final

# install git for future use
RUN apk add git

RUN mkdir -p /app && chown -R node:node /app

USER node
# set production environment
ENV NODE_ENV production

# set pm2 max instances
ENV PM2_MAX_INSTANCES max
# set pm2 error and out files
ENV PM2_ERROR_FILE "log/error.log"
ENV PM2_OUT_FILE "log/out.log"
# set remote address
ENV IP 0.0.0.0

WORKDIR /app

COPY --chown=node:node --from=builder /app /app

RUN npm run preinstall
# install production dependencies
RUN npm ci --only=production
# expose port
EXPOSE 5001

CMD ./node_modules/.bin/pm2-runtime --instances ${PM2_MAX_INSTANCES} --name api --error ${PM2_ERROR_FILE} --output ${PM2_OUT_FILE} bin/www
