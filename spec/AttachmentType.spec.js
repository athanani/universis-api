import app from '../server/app';
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../server/utils';
import AttachmentType from "../server/models/attachment-type-model";
const executeInTransaction = TestUtils.executeInTransaction;

describe('AttachmentType', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        return done();
    });
    afterAll( done => {
        if (context) {
            return context.finalize( ()=> done() );
        }
        return done();
    });

    it('should add physical attribute', async() => {
        await executeInTransaction(context, async () => {
            const newItem = {
                name: 'Student identification card',
                alternateName: 'StudentIdentificationCard',
                physical: true
            };
            await context.model(AttachmentType).silent().save(newItem);
            const item = await context.model(AttachmentType)
                .where('alternateName')
                .equal('StudentIdentificationCard')
                .silent()
                .getItem();
            expect(item).toBeTruthy();
            expect(item.physical).toBeTruthy();
        });
    });

});
