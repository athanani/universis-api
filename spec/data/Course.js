// eslint-disable-next-line no-unused-vars
import Course from '../../server/models/course-model';
import faker from 'faker';
import {DEPARTMENT} from './Department';
/**
 * @type Array<Course>
 */
const COURSES = [
    {
        id: '100000',
        displayCode: 'L100',
        name: 'History of Law',
        courseStructureType: 1, // a simple course without parts or parent course
        department: DEPARTMENT,
        courseUrl: faker.internet.url(),
        calculatedInRegistration: true,
        isEnabled: true,
        isLocal: true,
        isShared: false,
        units: 4,
        ects: 4,
        isCalculatedInScholarship: true,
        gradeScale: null
    },
    {
        id: '100001',
        displayCode: 'L101',
        name: 'Public International Law',
        courseStructureType: 1, // a simple course without parts or parent course
        department: DEPARTMENT,
        courseUrl: faker.internet.url(),
        calculatedInRegistration: true,
        isEnabled: true,
        isLocal: true,
        isShared: false,
        units: 5,
        ects: 5,
        isCalculatedInScholarship: true,
        gradeScale: null
    },
    {
        id: '100002',
        displayCode: 'L102',
        name: 'Constitutional Law',
        courseStructureType: 1, // a simple course without parts or parent course
        department: DEPARTMENT,
        courseUrl: faker.internet.url(),
        calculatedInRegistration: true,
        isEnabled: true,
        isLocal: true,
        isShared: false,
        units: 7,
        ects: 7,
        isCalculatedInScholarship: true,
        gradeScale: null
    },
    {
        id: '100003',
        displayCode: 'L103',
        name: 'General Principles of Civil Law',
        courseStructureType: 1, // a simple course without parts or parent course
        department: DEPARTMENT,
        courseUrl: faker.internet.url(),
        calculatedInRegistration: true,
        isEnabled: true,
        isLocal: true,
        isShared: false,
        units: 7,
        ects: 7,
        isCalculatedInScholarship: true,
        gradeScale: null
    },
    {
        id: '100004',
        displayCode: 'L104',
        name: 'Introduction to Legal Science',
        courseStructureType: 1, // a simple course without parts or parent course
        department: DEPARTMENT,
        courseUrl: faker.internet.url(),
        calculatedInRegistration: true,
        isEnabled: true,
        isLocal: true,
        isShared: false,
        units: 7,
        ects: 7,
        isCalculatedInScholarship: true,
        gradeScale: null
    }
];

export {
    COURSES
};
