
export * from './Institute';
export * from './Department';
export * from './Course';
export * from './StudyProgram';
export * from './GradeScale';
export * from './Utils';
