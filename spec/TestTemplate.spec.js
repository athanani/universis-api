import app from '../server/app';
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../server/utils';
const executeInTransaction = TestUtils.executeInTransaction;

describe('TestTemplate', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        return done();
    });
    afterAll( done => {
        if (context) {
            return context.finalize( ()=> done() );
        }
        return done();
    });

    it('should test app', async() => {
        await executeInTransaction(context, async () => {
            expect(context).toBeInstanceOf(ExpressDataContext);
            // write some code here
        });
    });

});
