import app from '../../server/app';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../../server/utils';
import { DefaultSchemaLoaderStrategy,
    SchemaLoaderStrategy,
    ODataModelBuilder,
    ODataConventionModelBuilder
 } from '@themost/data';
 import moment from 'moment';
 import path from 'path';
 import fs from 'fs';
 import { DocumentNumberService, DefaultDocumentNumberService } from '@universis/docnumbers';
import { PrivateContentService } from '../../server/services/content-service';
import { promisify } from 'util';
 // eslint-disable-next-line no-unused-vars
const executeInTransaction = TestUtils.executeInTransaction;

describe('DocumentNumberSeries', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        let configuration = app1.getConfiguration();
        // get loaders
        const loaders = configuration.getSourceAt('settings/schema/loaders') || [];
        // configure loaders
        const findLoader = loaders.find( x => {
            return x.loaderType === '@universis/docnumbers#DocumentNumberSchemaLoader';
        });
        if (findLoader == null) {
            loaders.push({
                loaderType: '@universis/docnumbers#DocumentNumberSchemaLoader'
            });
        }
        // use document service
        app1.useStrategy(DocumentNumberService, DefaultDocumentNumberService);
        configuration.setSourceAt('settings/schema/loaders', loaders);
        // reload SchemaLoaderStrategy strategy
        configuration.useStrategy(SchemaLoaderStrategy, DefaultSchemaLoaderStrategy);
        // reload ODataModelBuilder strategy
        configuration.useStrategy(ODataModelBuilder, ODataConventionModelBuilder);
        context = app1.createContext();
        return done();
    });
    afterAll( done => {
        if (context) {
            return context.finalize( ()=> done() );
        }
        return done();
    });

    it('should load model', async () => {
        const model = context.model('DepartmentDocumentNumberSeries');
        expect(model).toBeTruthy();
    });

    it('should add item', async () => {
        await executeInTransaction(context, async () => {
            // get department
            const department = await context.model('LocalDepartment').asQueryable().silent().getItem();
            let documentSeries = {
                department: department,
                name: `${department.name} Master`,
                lastIndex: 10,
                numberFormat: '2019/{00000}'
            }
            await context.model('DepartmentDocumentNumberSeries').silent().save(documentSeries);
            documentSeries = await context.model('DepartmentDocumentNumberSeries')
                .where('id').equal(documentSeries.id)
                .silent()
                .getItem();
            expect(documentSeries).toBeTruthy();
            expect(documentSeries.lastIndex).toBe(10);
        });
    });

    it('should add parent item', async () => {
        await executeInTransaction(context, async () => {

            let parentDocumentSeries = {
                name: `Institute Master`,
                lastIndex: 10,
                numberFormat: '2019/{00000}'
            }
            await context.model('InstituteDocumentNumberSeries').silent().save(parentDocumentSeries);
            const parentSeries = await context.model('InstituteDocumentNumberSeries')
                .where('id').equal(parentDocumentSeries.id)
                .silent().getItem();
            expect(parentSeries).toBeTruthy();
            const institute = await context.model('Institute')
                .where('local').equal(true)
                .silent().getItem();
            expect(institute).toBeTruthy();
            expect(parentSeries.institute).toBe(institute.id);

            // add child
            const department = await context.model('LocalDepartment').asQueryable().silent().getItem();
            let documentSeries = {
                department: department,
                name: `${department.name} Master`,
                lastIndex: 10,
                numberFormat: '2019/{00000}',
                parent: parentSeries,
                useParentIndex: true
            }
            await context.model('DepartmentDocumentNumberSeries').silent().save(documentSeries);
            documentSeries = await context.model('DepartmentDocumentNumberSeries')
                .where('id').equal(documentSeries.id)
                .silent()
                .getItem();
            expect(documentSeries).toBeTruthy();
            expect(documentSeries.parent).toBe(parentSeries.id);
            expect(documentSeries.useParentIndex).toBe(true);
        });
    });

    it('should get parent last index', async () => {
        await executeInTransaction(context, async () => {

            let parentDocumentSeries = {
                name: `Institute Master`,
                lastIndex: 20,
                numberFormat: '2020/{00000}'
            }
            await context.model('InstituteDocumentNumberSeries').silent().save(parentDocumentSeries);
            // add child
            const department = await context.model('LocalDepartment').asQueryable().silent().getItem();
            let documentSeries = {
                department: department,
                name: `${department.name} Master`,
                lastIndex: 0,
                numberFormat: '2019/{00000}',
                parent: parentDocumentSeries,
                useParentIndex: true
            }
            await context.model('DepartmentDocumentNumberSeries').silent().save(documentSeries);
            documentSeries = await context.model('DepartmentDocumentNumberSeries')
                .where('id').equal(documentSeries.id)
                .silent()
                .getTypedItem();
            const nextIndex = await documentSeries.next();
            expect(nextIndex).toBe(21);
            const parentSeries = await context.model('InstituteDocumentNumberSeries')
                .where('id').equal(parentDocumentSeries.id)
                .silent().getItem();
            expect(parentSeries.lastIndex).toBe(21);
            // get document series again
            documentSeries = await context.model('DepartmentDocumentNumberSeries')
                .where('id').equal(documentSeries.id)
                .silent()
                .getTypedItem();
            expect(documentSeries.lastIndex).toBe(0);
            delete documentSeries.useParentIndex;
            delete documentSeries.parent;
            const formattedIndex = await documentSeries.format(nextIndex);
            expect(formattedIndex).toBe('2020/00021')

        });
    });

    it('should update item', async () => {
        await executeInTransaction(context, async () => {
            // get department
            const department = await context.model('LocalDepartment').asQueryable().silent().getItem();
            let documentSeries = {
                department: department,
                name: `${department.name} Master`,
                lastIndex: 10,
                numberFormat: '2019/{00000}'
            }
            await context.model('DepartmentDocumentNumberSeries').silent().save(documentSeries);
            documentSeries = await context.model('DepartmentDocumentNumberSeries')
                .where('id').equal(documentSeries.id)
                .silent()
                .getItem();
            documentSeries.lastIndex = 11;
            await context.model('DepartmentDocumentNumberSeries').silent().save(documentSeries);
            documentSeries = await context.model('DepartmentDocumentNumberSeries')
                .where('id').equal(documentSeries.id)
                .silent()
                .getItem();
            expect(documentSeries).toBeTruthy();
            expect(documentSeries.lastIndex).toBe(11);
        });
        
    });

    it('should remove item', async () => {
        await executeInTransaction(context, async () => {
            // get department
            const department = await context.model('LocalDepartment').asQueryable().silent().getItem();
            let documentSeries = {
                department: department,
                name: `${department.name} Master`,
                lastIndex: 10,
                numberFormat: '2019/{00000}'
            }
            await context.model('DepartmentDocumentNumberSeries').silent().save(documentSeries);
            documentSeries = await context.model('DepartmentDocumentNumberSeries')
                .where('id').equal(documentSeries.id)
                .silent()
                .getItem();
            await context.model('DepartmentDocumentNumberSeries').silent().remove(documentSeries);
            documentSeries = await context.model('DepartmentDocumentNumberSeries')
                .where('id').equal(documentSeries.id)
                .silent()
                .getItem();
            expect(documentSeries).toBeFalsy();
            
        });
        
    });

    it('should get next index', async () => {
        await executeInTransaction(context, async () => {
            // get department
            const department = await context.model('LocalDepartment').asQueryable().silent().getItem();
            let documentSeries = {
                department: department,
                name: `${department.name} Master`,
                lastIndex: 10,
                numberFormat: '2020/{00000}'
            }
            await context.model('DepartmentDocumentNumberSeries').silent().save(documentSeries);
            documentSeries = await context.model('DepartmentDocumentNumberSeries')
                .where('id').equal(documentSeries.id)
                .silent()
                .getTypedItem();
            const nextIndex = await documentSeries.next();
            expect(nextIndex).toBe(11);
            const formattedIndex = await documentSeries.format(nextIndex);
            delete documentSeries.numberFormat;
            expect(formattedIndex).toBe('2020/00011');
        });
        
    });

    it('should format document number', async () => {
        await executeInTransaction(context, async () => {
            // get department
            let department = await context.model('LocalDepartment').asQueryable().silent().getItem();
            let documentSeries = {
                department: department,
                name: `${department.name} Master`,
                lastIndex: 10,
                numberFormat: '{AAAAA}/{00000}'
            }
            await context.model('DepartmentDocumentNumberSeries').silent().save(documentSeries);
            documentSeries = await context.model('DepartmentDocumentNumberSeries')
                .where('id').equal(documentSeries.id)
                .silent()
                .getTypedItem();
            let formattedNumber = await documentSeries.format(15);
            expect(formattedNumber).toBeTruthy();
            const academicYear = department.currentYear.id
            expect(formattedNumber).toBe(`${academicYear}-${(academicYear+1).toString().substr(2)}/00015`);

            const momentInstance = moment(new Date());
            documentSeries.numberFormat = '{YYYY}/{00000}';
            formattedNumber = await documentSeries.format(15);
            expect(formattedNumber).toBeTruthy();

            expect(formattedNumber).toBe(`${momentInstance.format('YYYY')}/00015`);

            documentSeries.numberFormat = '{YYYY}/{MM}/{00000}';
            formattedNumber = await documentSeries.format(15);
            expect(formattedNumber).toBeTruthy();
            expect(formattedNumber).toBe(`${momentInstance.format('YYYY')}/${momentInstance.format('MM')}/00015`);

            documentSeries.numberFormat = '{YYYY}/{DD}/{MM}/{00000}';
            formattedNumber = await documentSeries.format(15);
            expect(formattedNumber).toBeTruthy();
            expect(formattedNumber).toBe(`${momentInstance.format('YYYY')}/${momentInstance.format('DD')}/${momentInstance.format('MM')}/00015`);
            
        });
        
    });

    it('should add document', async () => {
        await executeInTransaction(context, async () => {
            // get department
            const department = await context.model('LocalDepartment').asQueryable().silent().getItem();
            let documentSeries = {
                department: department,
                name: `${department.name} Master`,
                lastIndex: 10,
                numberFormat: '2019/{00000}'
            }
            await context.model('DepartmentDocumentNumberSeries').silent().save(documentSeries);
            documentSeries = await context.model('DepartmentDocumentNumberSeries')
                .where('id').equal(documentSeries.id)
                .silent()
                .getTypedItem();
            // get user
            const user = await context.model('User').where('groups/name').equal('Administrators').silent().getItem();
            expect(user).toBeTruthy();
            context.user = user;
            /**
             * @type {DocumentNumberService}
             */
            const service = context.application.getService(DocumentNumberService);
            // get formatted document number
            const documentNumber = await documentSeries.nextAndFormat();
            const file = path.resolve(__dirname, '../files/LoremIpsum.pdf');
            let newItem = await service.add(context, file, {
                name: 'LoremIspum.pdf',
                contentType: 'application/pdf',
                parentDocumentSeries: documentSeries,
                documentNumber: documentNumber
            });
            const item = await context.model('DocumentNumberSeriesItem')
                .where('id').equal(newItem.id)
                .silent()
                .getItem();
            expect(item).toBeTruthy();
            expect(item.id).toBe(newItem.id);
        });
    });

    it('should add blob as document', async () => {
        await executeInTransaction(context, async () => {
            // get department
            const department = await context.model('LocalDepartment').asQueryable().silent().getItem();
            let documentSeries = {
                department: department,
                name: `${department.name} Master`,
                lastIndex: 10,
                numberFormat: '2019/{00000}'
            }
            await context.model('DepartmentDocumentNumberSeries').silent().save(documentSeries);
            documentSeries = await context.model('DepartmentDocumentNumberSeries')
                .where('id').equal(documentSeries.id)
                .silent()
                .getTypedItem();
            // get user
            const user = await context.model('User').where('groups/name').equal('Administrators').silent().getItem();
            expect(user).toBeTruthy();
            context.user = user;
            /**
             * @type {DocumentNumberService}
             */
            const service = context.application.getService(DocumentNumberService);
            // get formatted document number
            const documentNumber = await documentSeries.nextAndFormat();
            const file = path.resolve(__dirname, '../files/LoremIpsum.pdf');
            const fileStats = fs.statSync(file);
            // get blob
            const buffer = fs.readFileSync(file);
            // buffer to blob
            const blob = Uint8Array.from(buffer).buffer;
            let newItem = await service.addFrom(context, blob, {
                name: 'LoremIspum.pdf',
                contentType: 'application/pdf',
                parentDocumentSeries: documentSeries,
                documentNumber: documentNumber
            });
            const item = await context.model('DocumentNumberSeriesItem')
                .where('id').equal(newItem.id)
                .silent()
                .getItem();
            expect(item).toBeTruthy();
            expect(item.id).toBe(newItem.id);
            /**
             * @type {PrivateContentService}
             */
            const contentService = context.application.getService(PrivateContentService);
            const resolvePhysicalPathAsync = promisify(contentService.resolvePhysicalPath).bind(contentService);
            const physicalPath = await resolvePhysicalPathAsync(context, item);
            expect(physicalPath).toBeTruthy();
            expect(fs.existsSync(physicalPath)).toBeTruthy();
            const newFileStats = fs.statSync(physicalPath);
            expect(newFileStats.size).toBeGreaterThan(0);
            expect(newFileStats.size).toBe(fileStats.size);
        });
    });

    it('should replace document with blob', async () => {
        await executeInTransaction(context, async () => {
            // get department
            const department = await context.model('LocalDepartment').asQueryable().silent().getItem();
            let documentSeries = {
                department: department,
                name: `${department.name} Master`,
                lastIndex: 10,
                numberFormat: '2019/{00000}'
            }
            await context.model('DepartmentDocumentNumberSeries').silent().save(documentSeries);
            documentSeries = await context.model('DepartmentDocumentNumberSeries')
                .where('id').equal(documentSeries.id)
                .silent()
                .getTypedItem();
            // get user
            const user = await context.model('User').where('groups/name').equal('Administrators').silent().getItem();
            expect(user).toBeTruthy();
            context.user = user;
            /**
             * @type {DocumentNumberService}
             */
            const service = context.application.getService(DocumentNumberService);
            // get formatted document number
            const documentNumber = await documentSeries.nextAndFormat();
            let file = path.resolve(__dirname, '../files/LoremIpsum.pdf');
            let fileStats = fs.statSync(file);
            // get blob
            let buffer = fs.readFileSync(file);
            // buffer to blob
            let blob = Uint8Array.from(buffer).buffer;
            let newItem = await service.addFrom(context, blob, {
                name: 'LoremIspum.pdf',
                contentType: 'application/pdf',
                parentDocumentSeries: documentSeries,
                documentNumber: documentNumber
            });
            let item = await context.model('DocumentNumberSeriesItem')
                .where('id').equal(newItem.id)
                .silent()
                .getItem();
            expect(item).toBeTruthy();
            expect(item.id).toBe(newItem.id);

            // replace
            file = path.resolve(__dirname, '../files/StudentCertificate.pdf');
            fileStats = fs.statSync(file);
            buffer = fs.readFileSync(file);
            blob = Uint8Array.from(buffer).buffer;
            const saveItem = Object.assign({}, item, {
                name: 'StudentCertificate.pdf'
            });
            await service.replaceFrom(context, blob, saveItem);

            item = await context.model('DocumentNumberSeriesItem')
                .where('id').equal(saveItem.id)
                .silent()
                .getItem();
            expect(item).toBeTruthy();
            expect(item.id).toBe(newItem.id);

            /**
             * @type {PrivateContentService}
             */
            const contentService = context.application.getService(PrivateContentService);
            const resolvePhysicalPathAsync = promisify(contentService.resolvePhysicalPath).bind(contentService);
            const physicalPath = await resolvePhysicalPathAsync(context, item);
            expect(physicalPath).toBeTruthy();
            expect(fs.existsSync(physicalPath)).toBeTruthy();
            const newFileStats = fs.statSync(physicalPath);
            expect(newFileStats.size).toBeGreaterThan(0);
            expect(newFileStats.size).toBe(fileStats.size);
        });
    });

});
