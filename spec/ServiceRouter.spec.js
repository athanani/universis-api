import app from '../server/app';
import request from 'supertest'
describe('ServiceRouter', () => {

    it('should call public endpoint /api/', async () => {
        let response = await request(app)
            .get('/api/')
        expect(response.status).toBe(200);
        expect(response.body).toBeTruthy();
        response = await request(app)
            .get('/api')
        expect(response.status).toBe(200);
        expect(response.body).toBeTruthy();
        response = await request(app)
            .post('/api/')
        expect(response.status).toBe(401);
    });

    it('should call public endpoint /api/$metadata', async () => {
        // noinspection JSCheckFunctionSignatures
        let response = await request(app)
            .get('/api/$metadata')
        expect(response.status).toBe(200);
        expect(response.type).toBe('application/xml');
    });

    it('should get 401 Unauthorized for /api/Students/ as anonymous', async () => {
        // noinspection JSCheckFunctionSignatures
        let response = await request(app)
            .get('/api/Students/')
        expect(response.status).toBe(401);
    });

});
