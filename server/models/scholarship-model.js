import {EdmMapping, DataObject, EdmType, DataPermissionEventListener, DataObjectState} from "@themost/data";
import {promisify} from "es6-promisify";
import {ValidationResult} from "../errors";
import _ from "lodash";
import {TraceUtils} from "@themost/common/utils";
const Rule = require('./rule-model');
/**
 * @class

 * @property {number} id
 * @property {Department|any} department
 * @property {string} name
 * @property {Company|any} organization
 * @property {string} contactName
 * @property {AcademicYear|any} year
 * @property {number} totalStudents
 * @property {string} notes
 * @property {ScholarshipType|any} scholarshipType
 * @property {ScholarshipStatus|any} status
 * @property {Date} dateModified
 * @augments {DataObject}
 */
@EdmMapping.entityType('Scholarship')
class Scholarship extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    async updateTotalStudents() {
        this.totalStudents = await this.context.model('StudentScholarship').where('scholarship').equal(this.getId()).count();
        await this.save();
    }

    async validate(student) {
        const context = this.context;
        const st = await context.model('Student').where("id").equal(student).silent().getTypedItem();
        const data = {
            student: st
        };
        // validate scholarship rules
        const rules = await context.model('Rule').where('additionalType').equal('ScholarshipRule')
            .and('targetType').equal('Scholarship')
            .and('target').equal(this.getId().toString()).getItems();

        if (rules && rules.length === 0) {
            return;
        }
        let validationResults = [];
        // add async function for validating graduation rules
        let forEachRule = (scholarshipRule) => {
            try {
                return new Promise((resolve) => {
                    const ruleModel = context.model(scholarshipRule.refersTo + 'Rule');
                    if (_.isNil(ruleModel)) {
                        validationResults = validationResults || [];
                        const errorResult = new ValidationResult(false, 'FAIL', context.__('Student validation rule type cannot be found.'));
                        validationResults.push(errorResult);
                        scholarshipRule.validationResult = errorResult;
                        return resolve();
                    }
                    const rule = ruleModel.convert(scholarshipRule);
                    rule.validate(data, function (err, result) {
                        if (err) {
                            validationResults = validationResults || [];
                            const errorResult = new ValidationResult(false, 'FAIL', err.message);
                            validationResults.push(errorResult);
                            scholarshipRule.validationResult = errorResult;
                            return resolve();
                        }
                        /**
                         * @type {ValidationResult[]}
                         */
                        validationResults = validationResults || [];
                        validationResults.push(result);
                        scholarshipRule.validationResult = result;
                        return resolve();
                    });
                });
            } catch (err) {
                TraceUtils.error(err);
            }
        };
        for (let rule of rules) {
            await forEachRule(rule);
        }

        const fnValidateExpression = function (x) {
            try {
                let expr = x['ruleExpression'];
                if (_.isEmpty(expr)) {
                    return false;
                }
                expr = expr.replace(/\[%(\d+)]/g, function () {
                    if (arguments.length === 0) return;
                    const id = parseInt(arguments[1]);
                    const v = validationResults.find(function (y) {
                        return y.id === id;
                    });
                    if (v) {
                        return v.success.toString();
                    }
                    return 'false';
                });
                expr = expr.replace(/\bAND\b/ig, ' && ').replace(/\bOR\b/ig, ' || ').replace(/\bNOT\b/ig, ' !');
                return eval(expr);
            } catch (e) {
                return e;
            }
        };

        const fnTitleExpression = function (x) {
            try {
                let expr = x['ruleExpression'];
                if (_.isEmpty(expr)) {
                    return false;
                }
                expr = expr.replace(/\[%(\d+)]/g, function () {
                    if (arguments.length === 0) return;
                    const id = parseInt(arguments[1]);
                    const v = validationResults.find(function (y) {
                        return y.id === id;
                    });
                    if (v) {
                        return '(' + v.message.toString() + ')';
                    }
                    return 'Unknown Rule';
                });
                expr = expr.replace(/\bAND\b/ig, context.__(' AND ')).replace(/\bOR\b/ig, context.__(' OR ')).replace(/\bNOT\b/ig, context.__(' NOT '));
                return expr;
            } catch (e) {
                return e;
            }
        };

        let res, title;

        //apply default expression
        const expr = rules.find(function (x) {
            return !_.isEmpty(x['ruleExpression']);
        });
        let finalResult;
        if (expr) {
            res = fnValidateExpression(expr);
            title = fnTitleExpression(expr);
            finalResult = new ValidationResult(res, res === true ? 'SUCC' : 'FAIL', title);
            finalResult.type = 'ScholarshipRule';
            expr.ruleExpression = expr.ruleExpression.replace(/\bAND\b/ig, ' && ').replace(/\bOR\b/ig, ' || ').replace(/\bNOT\b/ig, ' !');
            expr.ruleExpression = expr.ruleExpression.replace(/\[%(\d+)]/g, function () {
                if (arguments.length === 0) return;
                return parseInt(arguments[1]);
            });

        } else {
            //get expression (for this rule type)
            const ruleExp1 = rules.map(function (x) {
                return '[%' + x.id + ']';
            }).join(' AND ');
            res = fnValidateExpression({ruleExpression: ruleExp1});
            title = fnTitleExpression({ruleExpression: ruleExp1});
            finalResult = new ValidationResult(res, res === true ? 'SUCC' : 'FAIL', title);
            finalResult.type = 'ScholarshipRule';
            finalResult.expression = null;
        }
        // add data result to finalResult from rules MeanGrade, YearMeanGrade
        const resultData = rules.find(x => {
            return x.refersTo === 'MeanGrade' || x.refersTo === 'YearMeanGrade';
        });
        finalResult.data = resultData && resultData.validationResult ? resultData.validationResult.data : {};
        return {finalResult, "scholarshipRules": rules};
    }


    /**
     * Returns a collection of rules which are going to be checked while validating a student scholarship
     * @returns {Promise<any[]>}
     */
    @EdmMapping.func('ScholarshipRules', EdmType.CollectionOf('Rule'))
    async getScholarshipRules() {
        // return rules of type ScholarshipRule
        return Rule.expand(this.context, this.getId().toString(),
            'Scholarship', 'ScholarshipRule');
    }

    /**
     * Updates scholarship rules
     * @returns {Promise<any[]>}
     */
    @EdmMapping.param('items', EdmType.CollectionOf('Rule'), false, true)
    @EdmMapping.action('ScholarshipRules', EdmType.CollectionOf('Rule'))
    async setScholarshipRules(items) {
        const finalItems = [];
        for (let item of items) {
            // convert item
            const converted = await Rule.flatten(this.context, item, `${item.refersTo}RuleEx`);
            // set target id
            converted.target = this.getId().toString();
            // set target type
            converted.targetType = 'Scholarship';
            // set additional type
            converted.additionalType = 'ScholarshipRule';
            // add item
            finalItems.push(converted);
        }
        // validate permissions
        const validateAsync = promisify(DataPermissionEventListener.prototype.validate);
        await validateAsync({
            model: this.getModel(), // set current model
            state: DataObjectState.Update, // set state to update
            target: this // this object is the target object
        });
        // save items
        await this.context.model(Rule).silent().save(finalItems);
        // and return new collection
        return this.getScholarshipRules();
    }

    /**
     * Updates scholarship rules
     * @returns {Promise<any[]>}
     */
    @EdmMapping.param('items', EdmType.CollectionOf('Student'), false, true)
    @EdmMapping.action('checkStudents', 'ScholarshipValidateAction')
    async checkStudents(items) {
        try {
            // check all students
            const action = await this.context.model('ScholarshipValidateAction').silent().save(
                {
                    scholarship: this.getId(),
                    totalStudents: items.length
                });
            await this.checkRules(this.context, this.getId(), items, action);
            return action;
        } catch (err) {
            throw (err);
        }
    }

    checkRules(appContext, scholarship, students, action) {
        const app = appContext.getApplication();
        const context = app.createContext();

        (async function () {
            // check rules for each student
            // validate scholarship rules
            /**
             * @type {Scholarship}
             */
            scholarship = context.model('Scholarship').convert(scholarship);
            for (let i = 0; i < students.length; i++) {
                let student = students[i];
                try {
                    await new Promise((resolve, reject) => {
                        return scholarship.validate(student.id).then(validationResult => {
                            const result = {
                                action: action.id,
                                scholarship: scholarship,
                                student: student.id
                            };
                            if (validationResult && validationResult.finalResult) {
                                result.result = validationResult.finalResult.success;
                                result.grade = validationResult.finalResult.data && validationResult.finalResult.data.finalGrade;

                            } else {
                                result.result = false;
                                TraceUtils.error('Could not validate student scholarship');
                            }
                            return context.model('StudentScholarshipValidateResult').silent().save(result).then(res => {
                                return resolve();
                            })
                        }).catch(err => {
                            return resolve();
                        });


                    });

                } catch (err) {
                    TraceUtils.error(err);
                }
            }
        })().then(() => {
            context.finalize(() => {
                // build result

                // after finishing sending mails, an instructor message is created to inform instructor
                action.actionStatus = {alternateName: 'CompletedActionStatus'};
                action.endTime = new Date();
                return context.model('ScholarshipValidateAction').silent().save(action).then(action => {
                }).catch(err => {
                    TraceUtils.error(err);
                });
            });
        }).catch(err => {
            context.finalize(() => {
                action.actionStatus = {alternateName: 'FailedActionStatus'};
                action.endTime = new Date();
                action.description = err.message;
                return context.model('ScholarshipValidateAction').silent().save(action).then(action => {
                }).catch(err => {
                    TraceUtils.error(`An error occurred while checking rules for scholarship with id ${scholarship}`);
                    TraceUtils.error(err);
                });
            });
        });
    }
}

module.exports = Scholarship;

