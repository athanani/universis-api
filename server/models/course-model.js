import _ from 'lodash';
import {TraceUtils, LangUtils} from '@themost/common/utils';
import async from 'async';
import {DataObject} from "@themost/data/data-object";
import {HttpServerError, DataError, DataNotFoundError} from "@themost/common/errors";
import {DataConflictError} from "../errors";
import {EdmMapping} from "@themost/data";

/**
 * @class
 * @param {*} obj
 * @augments DataObject
 */
class Course extends DataObject {
    constructor() {
        super();
    }

    passed(student, callback) {
        const self = this, context = self.context;
        try {
            if (_.isNil(student)) {
                return callback(null);
            }
            const studentCourses = context.model('StudentCourse'), courses = context.model('Course');
            studentCourses.where('student').equal(student).and('course').equal(self.id).and('isPassed').equal(1).silent().count(function(err,count) {
                if (err) { return callback(err);}
                if (count===1) {
                    return callback(null, true);
                }
                courses.where('id').equal(self.id).and('replacedCourse').notEqual(null).select('replacedCourse').silent().flatten().first(function(err, result) {
                    if (err) { return callback(err);}
                    if (_.isNil(result)) {
                        return callback(null, false);
                    }
                    const replacedCourse = result['replacedCourse'], arr = [];
                    if (typeof replacedCourse === 'string' && replacedCourse.length>0) {
                        arr.push.call(arr, replacedCourse.split(','));
                    }
                    if (arr.length===0) {
                        return callback(null, false);
                    }

                    return courses.where('id').in(arr).select('id').silent().getAllTypedItems().then((result) => {
                        let res = false;
                        return (async () => {
                            for (let i = 0; i < result.length; i++) {
                                await new Promise((resolve, reject) => {
                                    /**
                                     * @type {Course}
                                     */
                                    let course = result[i];
                                    course.passed(student,function(err, value) {
                                        if (value) {
                                            res = value;
                                            return resolve(null, false);
                                        }
                                        else {
                                            return resolve();
                                        }
                                    });
                                });
                            }
                        })().then(()=> {
                            return callback(null, res);
                        }).catch(err=> {
                            if (err && err.code !== 'ABRK') {
                                return callback(err);
                            }
                            return callback(null, res);
                        });

                    }).catch(err => {
                        return callback(err);
                    });
                });
            });
        }
        catch (err) {
            callback(err);
        }
    }

    replacedPassed(student, callback) {
        const self = this, context = self.context;
        try {
            if (_.isNil(student)) {
                return callback(null);
            }
            //var studentCourses = context.model('StudentCourse'),
            const courses = context.model('Course');
            courses.where('id').equal(self.id).and('replacedCourse').notEqual(null).select(['replacedCourse']).silent().flatten().first(function (err, result) {
                if (err) {
                    return callback(err);
                }
                if (_.isNil(result)) {
                    return callback(null, false);
                }
                const replacedCourse = result['replacedCourse'], arr = [];
                if (typeof replacedCourse === 'string' && replacedCourse.length > 0) {
                    arr.push.call(arr, replacedCourse.split(','));
                }
                if (arr.length === 0) {
                    return callback(null, false);
                }
                courses.where('id').in(arr).select('id').flatten().silent().all(function (err, result) {
                    if (err) {
                        return callback(err);
                    }
                    if (result.length === 0) {
                        return callback(null, false);
                    }
                    let res = false, course;
                    async.eachSeries(result, function (item, cb) {
                        course = courses.convert(item);
                        course.passed(student, function (err, value) {
                            if (value) {
                                res = value;
                                const er = new Error('Break');
                                er.code = 'ABRK';
                                cb(er);
                            }
                            else {
                                cb();
                            }
                        });
                    }, function (err) {
                        if (err && err.code !== 'ABRK') {
                            return callback(err);
                        }
                        callback(null, res);
                    });
                });
            });
        }
        catch (e) {
            callback(e);
        }
    }

    lastGrades(callback) {
        const self = this, context = self.context;
        //get course
        context.model('Course').where('id').equal(self.id).select('id','department').flatten().first(function(err, course) {
            if (err) {
                return callback(err);
            }
            else if (_.isNil(course)) {
                return callback(null, self.json([]));
            }
            else {
                //get course department and check if showing grades is allowed.
                context.model('LocalDepartment').where('id').equal(course.department).select('id','webShowGrades','webResultsYear','currentYear').first(function(err, department) {
                    if (err) {
                        return callback(err);
                    }
                    if (_.isNil(department)) {
                        return callback(null, self.json([]));
                    }
                    if (LangUtils.parseInt(department.webShowGrades)===0)
                    {
                        //department does not allow showing grades
                        return callback(null, {error: 'Course department does not allow showing grades'});
                    }

                    const resultYear=department.webResultsYear || department.currentYear;
                    //get request params
                    const params = Object.assign({}, context.params);
                    delete params.id;
                    //get student grades for specific year in silent mode
                    context.model('StudentCourse').filter(params, function (err, q) {
                        q.where('course').equal(self.id).and('gradeYear').equal(resultYear);
                        q.select('student/studentIdentifier as studentIdentifier', 'gradePeriodDescription', 'formattedGrade', 'gradeYear');
                        const $top = LangUtils.parseInt(context.params.$top);
                        if ($top > 0) {
                            q.take($top).silent().list(function (err, result) {
                                if (err) {
                                    TraceUtils.error(err);
                                    return callback(new HttpServerError());
                                }
                                callback(null, {grades: result.records, total: result.total});
                            });
                        }
                        else {
                            q.silent().all(function (err, result) {
                                if (err) {
                                    TraceUtils.error(err);
                                    return callback(new HttpServerError());
                                }
                                callback(null, {grades: result});
                            });
                        }
                    });
                });
            }
        });
    }
    /**
     * Updates student course attributes
     * @returns {Promise<any[]>}
     */
    @EdmMapping.param('data','Object', false, true)
    @EdmMapping.action('updateStudents', 'CourseUpdateAction')
    async updateStudents(data) {
        try {
            if (Object.keys(data.attributes).length > 0) {
                let description ='';
                    Object.keys(data.attributes).forEach(e => {
                    const value = (typeof data.attributes[e] === 'object' && data.attributes[e].id != null) ? data.attributes[e].name : data.attributes[e];
                    return description +=  this.context.__(e) + ' ' + value  + ',';
                });
                const action = await this.context.model('CourseUpdateAction').silent().save(
                    {
                        course: this.getId(),
                        totalStudents: data.students.length,
                        description: description
                    });
                await this.updateCourseAttributes(this.context, this.getId(), data.students, action, data.attributes);
                return action;
            }
            else
            {
                throw new DataConflictError(this.context.__('Course attributes not set'));
            }
        } catch (err) {
            throw (err);
        }
    }

    updateCourseAttributes(appContext, course, students, action, params) {
        const app = appContext.getApplication();
        const context = app.createContext();
        context.user = appContext.user;
        (async function () {
            // update course attributes for each student
            /**
             * @type {Course}
             */
            course = context.model('Course').convert(course);
            for (let i = 0; i < students.length; i++) {
                let student = students[i];
                try {
                    await new Promise((resolve, reject) => {
                        return context.model('StudentCourse')
                            .where('student').equal(student.id).and('course')
                            .equal(course.id)
                            .and('student/studentStatus/alternateName').equal('active')
                            .expand('courseType','semester')
                            .getItem().then(studentCourse => {
                            const result = {
                                action: action && action.id,
                                course: course.id,
                                student: student.id,
                                description: '',
                                updated: true
                            };
                            if (studentCourse) {
                                let shouldBeUpdated = false;
                                // check which attribute changed

                                Object.keys(params).forEach(param => {
                                    let paramDescription = typeof params[param] ==='object' ?params[param].name: params[param];
                                    const paramValue =  typeof params[param] ==='object' ?params[param].id: params[param];
                                    if ((typeof studentCourse[param]==='object' && studentCourse[param]!= null && studentCourse[param].id!= paramValue)
                                    || ((typeof studentCourse[param]!=='object' ||  studentCourse[param]== null ) && studentCourse[param]!=paramValue)) {
                                        shouldBeUpdated = true;
                                        // get model attribute type
                                        let prevValue =studentCourse[param]== null? '-': typeof studentCourse[param]==='object'? studentCourse[param].name:studentCourse[param];
                                        const attr =  context.model('StudentCourse').getAttribute(param);
                                        if (attr && attr.type==='Boolean') {
                                            prevValue = studentCourse[param] === 1 ? context.__('Yes') : context.__('No');
                                            paramDescription =  LangUtils.parseBoolean(params[param]) ? context.__('Yes') : context.__('No');
                                        }
                                        // create change description
                                        result.description += `${context.__(param)}: ${prevValue} => ${paramDescription}, `;
                                        studentCourse[param] = params[param];
                                    }
                                });
                                if (shouldBeUpdated) {
                                    // remove last comma
                                    result.description = result.description.replace(/,\s*$/, "");
                                    return context.model('StudentCourse').save(studentCourse).then(res => {
                                        return context.model('StudentCourseUpdateResult').silent().save(result).then(res => {
                                            return resolve();
                                        });
                                    });
                                }
                                else
                                {
                                    // nothing to update
                                    result.updated = false;
                                    result.description = context.__('Student course has already these attributes');
                                    return context.model('StudentCourseUpdateResult').silent().save(result).then(res => {
                                        return resolve();
                                    });
                                }
                            }
                            else
                            {
                                // student course not found or student is not active
                                result.description = action.description;
                                result.result = context.__('Student course not found or student is not active');
                                result.updated = false;
                                return context.model('StudentCourseUpdateResult').silent().save(result).then(res => {
                                    return resolve();
                                });
                            }
                        }).catch(err => {
                            return resolve();
                        });
                    });

                } catch (err) {
                    TraceUtils.error(err);
                }
            }
        })().then(() => {
            context.finalize(() => {
                // build result

                // after finishing sending mails, an instructor message is created to inform instructor
                action.actionStatus = {alternateName: 'CompletedActionStatus'};
                action.endTime = new Date();
                return context.model('CourseUpdateAction').silent().save(action).then(action => {
                }).catch(err => {
                    TraceUtils.error(err);
                });
            });
        }).catch(err => {
            context.finalize(() => {
                action.actionStatus = {alternateName: 'FailedActionStatus'};
                action.endTime = new Date();
                action.description = err.message;
                return context.model('CourseUpdateAction').silent().save(action).then(action => {
                }).catch(err => {
                    TraceUtils.error(`An error occurred while updating student course for course with id ${course}`);
                    TraceUtils.error(err);
                });
            });
        });
    }

    /**
     * @returns {CourseClass}
     */
     @EdmMapping.param("data", "Object", false, true)
     @EdmMapping.action('createClass', 'CourseClass')
     async createCourseClass(data) {
        const context = this.context;
        // validate course (permissions/existance)
        const course = await context.model('Course')
            .where('id').equal(this.getId())
            .select('id', 'instructor', 'name', 'department')
            .getItem();
        if (course == null) {
            throw new DataNotFoundError('The specified course cannot be found or is inaccessible.');
        }
        // validate data
        if (!data || data.year == null || data.period == null) {
            throw new DataError('Cannot create course class. Academic year and period must be provided.')
        }
        // check if class already exists - use silent to ensure it is found (although at this point permissions are probably ok)
        const existingCourseClass = await context.model('CourseClass')
            .where('course').equal(course.id)
            .and('year').equal(data.year)
            .and('period').equal(data.period)
            .silent()
            .getItem();
        if (existingCourseClass) {
            throw new DataError(`Course class for course ${course.name || course.id}, year ${data.year} and period ${data.period === 1 ? 'winter' : 'summer'} already exists.`);
        }
        // create course class
        const newCourseClass = {
            title: course.name,
            course: course.id,
            year: data.year,
            period: data.period,
            department: course.department,
            status: {
                alternateName: 'open'
            }
        }
        // add also course instructor to courseClassInstructors
        // and let course-class-listener handle it
        if (course.instructor) {
            Object.assign(newCourseClass, {
                instructors: [
                    {
                        instructor: course.instructor
                    }
                ]
            })
        }
        // and save
        return await context.model('CourseClass').save(newCourseClass);
     }
}

module.exports = Course;
