import {EdmMapping,EdmType} from '@themost/data';
import Action from './action-model';
/**
 * @class
 */
@EdmMapping.entityType('UploadAction')
class UploadAction extends Action {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * Adds an attachment
     * @param {*} file
     * @param {*=} extraAttributes
     */
    @EdmMapping.param('extraAttributes', 'Object', true, true)
    @EdmMapping.param('file', EdmType.EdmStream, false)
    @EdmMapping.action('AddAttachment', 'Attachment')
    async addAttachment(file, extraAttributes) {
        return await super.addAttachment(file, extraAttributes);
    }
    /**
     * Removes an attachment
     * @param {*} attachment
     */
    @EdmMapping.param('attachment', 'Attachment', true, true)
    @EdmMapping.action('RemoveAttachment', 'Attachment')
    async removeAttachment(attachment) {
        return await super.removeAttachment(attachment);
    }
}
module.exports = UploadAction;
