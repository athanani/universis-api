import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {number} id
 * @property {CourseExam|any} courseExam
 * @property {User|any} user
 * @property {string} documentSubject
 * @property {string} originalDocument
 * @property {string} signedDocument
 * @property {string} signatureBlock
 * @property {string} userCertificate
 * @property {DocumentStatus|any} documentStatus
 * @property {string} documentStatusReason
 * @property {string} checkHashKey
 * @property {Date} dateCreated
 * @property {Date} dateModified
 * @property {string} identifier
 * @augments {DataObject}
 */
@EdmMapping.entityType('CourseExamDocument')
class CourseExamDocument extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = CourseExamDocument;