import {HttpResponseFormatter} from "@themost/express";
import {TraceUtils} from "@themost/common/utils";

export class IcsFormatter extends HttpResponseFormatter {
    execute(req, res) {
        if (this.data && this.data.value && Array.isArray(this.data.value))
            return res.ics(this.data.value);
        else
            return res.ics(this.data);
    }
}
