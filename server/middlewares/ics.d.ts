import {RequestHandler} from "express";

export declare function icsParser(options?: any): RequestHandler;
export declare const icsContentType: string;
