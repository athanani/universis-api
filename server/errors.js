
import {DataError, HttpError} from '@themost/common';

/**
 * @class RegistrationError
 * @param {string=} message
 * @param {string=} innerMessage
 * @constructor
 * @augments {HttpError}
 */
export class RegistrationError extends HttpError {
 constructor(message, innerMessage) {
     super(500, message || 'An error occured while trying to register course.', innerMessage);
 }
}

/**
 * @class ProgramCourseNotFound
 * @param {string=} message
 * @param {string=} innerMessage
 * @constructor
 * @augments {HttpError}
 * @augments {Error}
 */
export class ProgramCourseNotFound extends HttpError {
 constructor(message, innerMessage) {
     super(404, message || 'The specified course cannot be found in program courses.', innerMessage);
 }
}

/**
 * @class ProgramCourseNotFound
 * @param {string=} message
 * @param {string=} innerMessage
 * @constructor
 * @augments {HttpError}
 * @augments {Error}
 */
export class  StudentNotFound extends HttpError {
 constructor(message, innerMessage) {
     super(404, message || 'Student data is missing or cannot be found.', innerMessage);
 }
}


export class  GenericDataConflictError extends DataError {
    /**
     * @param {string} code
     * @param {string=} message
     * @param {string=} innerMessage 
     * @param {string=} model 
     */
    constructor(code, message, innerMessage, model) {
        super(code, message || 'An error occurred while validating the state of the target object.', innerMessage, model);
        this.statusCode = 409;
    }
}

export class  DataConflictError extends GenericDataConflictError {
    /**
     * @param {string=} message 
     * @param {string=} innerMessage 
     * @param {string=} model 
     */
    constructor(message, innerMessage, model) {
        super('E_CONFLICT', message, innerMessage, model);
    }
}

/**
 * @class
 * @property {boolean} success
 * @property {string} code
 * @property {number} statusCode
 * @property {string} message
 * @property {string} innerMessage
 * @property {Array<any>} data
 * @property {Array<ValidationResult>} validationResults
 */
export class ValidationResult  {
    /**
     * @param {boolean} success
     * @param {string=} code
     * @param {string=} message
     * @param {string=} innerMessage
     * @param {any=} data
     */
    constructor(success, code, message, innerMessage, data) {
        Object.defineProperty(this, 'type', {
            value: this.constructor.name,
            writable: true,
            enumerable: true
        });
        this.success = typeof success !== 'undefined' && success !== null ? success : false;
        this.statusCode = this.success ? 200 : 422;
        this.code = code;
        this.message = message;
        this.innerMessage = innerMessage;
        this.data = data;
    }
}
