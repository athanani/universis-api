import ActionStatusType from '../models/action-status-type-model';
import {DataError} from "@themost/common";

/**
 * @async
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
    if (event.state !== 1) {
        return;
    }
    const target = event.model.convert(event.target);
    const context = event.model.context;
    // build description

    const student = await context.model('Student').where('id').equal(target.object).select('id',
        'studyProgram/name as studyProgram', 'studentIdentifier', 'studyProgramSpecialty/name as studyProgramSpecialty',
        'person/familyName as familyName', 'person/givenName as givenName').getItem();
    const newSpecialty = await context.model('StudyProgramSpecialty').where('id').equal(target.specialty).select('id',
        'studyProgram/name as studyProgram', 'name').getItem();
    if (student) {
        event.target.description = `${context.__('Change study program')}: ${student.studentIdentifier} ${student.familyName} ${student.givenName} (${student.studyProgram}-${student.studyProgramSpecialty}) ->  (${newSpecialty.studyProgram}-${newSpecialty.name})`;
    }
}

/**
 * @async
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    if (event.state === 4) {
        return;
    }
    const target = event.model.convert(event.target);
    const context = event.model.context;
    // get action status
    const actionStatus = await target.property('actionStatus').getItem();
    // if action status is other that active
    if (actionStatus.alternateName !== ActionStatusType.CompletedActionStatus) {
        // exit
        return;
    }

    // 1. check if student is active
    // 2. check if new study program is active
    // 3. check if study program belongs to student department
    // 4. check student specialty and related study program specialty
    // 5. update student courses with new course properties
    // 6. update student courses not belonging to new study program (calculateUnits, calculateGrade)
    // execute action by assigning student and specialty to an instance of StudentSpecialty class
    // delete old studentProgramGroups and try to assign new program groups

    // get student
    const student = await context.model('Student').where('id').equal(target.object).select('id', 'studentStatus', 'studyProgram/id as studyProgram').getTypedItem();
    if (student && student.studentStatus && student.studentStatus.alternateName !== 'active') {
        throw new DataError('ERR_STATUS',
            'Invalid student status. Student update program action may execute upon an active student only.'
            , null,
            'Student', 'studentStatus');
    }

    /**
     * @type {studyProgram}
     */
    const studyProgram = await context.model('StudyProgram').where('id').equal(target.studyProgram).getItem();

    if (student.studyProgram === studyProgram.id) {
        throw new DataError('ERR_STATUS',
            'Student already attends this program.'
            , null,
            'Student', 'studyProgram');
    }

    /**
     * @type {studyProgramSpecialty}
     */
    const studyProgramSpecialty = await context.model('StudyProgramSpecialty').where('id').equal(target.specialty).getItem();

    // save student attributes
    // get main studyProgramSpecialty for new studyProgram
    const mainSpecialty = await context.model('StudyProgramSpecialty').where('studyProgram').equal(studyProgram.id).and('specialty').equal(-1).getItem();
    student.studyProgram = studyProgram;
    student.studyProgramSpecialty = mainSpecialty.id;
    student.specialtyId = -1;
    // save student with main specialty
    await context.model('Student').save(student);

    if (mainSpecialty.id !== studyProgramSpecialty.id) {
        // save student specialty
        const data = {
            "object": student.id,
            "specialty": studyProgramSpecialty.id,
            "initiator": event.target.id
        };
        await context.model('UpdateSpecialtyAction').save(data);
    }

    // check if student has programGroups
    let studentProgramGroups = await context.model('StudentProgramGroup').where('student').equal(student.id).expand('ProgramGroup').getItems();
    if (studentProgramGroups && studentProgramGroups.length) {
        studentProgramGroups = studentProgramGroups.map(x => {
            x.$state = 4;
            return x;
        });
        // check if new studyProgram has programGroups with the same name

        const studentGroups = [];
        const availableProgramGroups = await student.availableProgramGroups();
        if (availableProgramGroups.length) {
            let programGroups = [];
            availableProgramGroups.map(programGroup => {
                programGroups = programGroups.concat(programGroup.groups);
            });

            for (let group of studentProgramGroups) {
                // check if group exists in student available groups
                let studentGroup = programGroups.find(programGroup => {
                    return group.programGroup.name === programGroup.name;
                });
                if (studentGroup) {
                    // validate programGroup rules
                    /**
                     * @type {ProgramGroup}
                     */
                    studentGroup = context.model('ProgramGroup').convert(studentGroup);
                    // validate program group rules
                    const validationResult = await studentGroup.validate(student.id);
                    if (validationResult && validationResult.finalResult) {
                        if (validationResult.finalResult.success === true) {
                            studentGroups.push(studentGroup);
                        }
                    } else {
                        studentGroups.push(studentGroup);
                    }
                }
            }
            if (studentGroups.length > 0) {
                await context.model('UpdateStudentGroupAction').save(
                    {
                        "object": student.id,
                        "groups": studentGroups,
                        "initiator": event.target.id
                    });
            }
        }

        // remove old studentProgramGroups
        await context.model('StudentProgramGroups').remove(studentProgramGroups);
    }

    if (event.target.updateCourse || event.target.excludeCourse) {
        // get student courses and change properties from new program course
        /**
         * @type {StudentCourse[]}
         */
        const studentCourses = await context.model('StudentCourse').where('student').equal(student.id).silent().flatten().getAllItems();
        if (studentCourses.length) {
            let updateCourses = [];
            /**
             * @type {SpecializationCourse[]}
             */
            const programCourses = await context.model('SpecializationCourse').where('studyProgramCourse/studyProgram').equal(studyProgram.id).equal(studyProgram.id)
                .and('specializationIndex').in(['-1', studyProgramSpecialty.specializationIndex]).expand('studyProgramCourse')
                .orderBy('specializationIndex')
                .getAllItems();
            // change course properties
            studentCourses.forEach(studentCourse => {
                const programCourse = programCourses.find(x => {
                    return x.studyProgramCourse && x.studyProgramCourse.course === studentCourse.course;
                });
                if (programCourse) {
                    if (event.target.updateCourse) {
                        // update student course attributes
                        // check if attributes are different
                        if (studentCourse.semester !== programCourse.semester ||
                            studentCourse.units !== programCourse.units ||
                            studentCourse.ects !== programCourse.ects ||
                            studentCourse.coefficient !== programCourse.coefficient ||
                            studentCourse.programGroup !== programCourse.studyProgramCourse.programGroup ||
                            studentCourse.groupPercent !== programCourse.studyProgramCourse.programGroupFactor ||
                            studentCourse.courseType !== programCourse.courseType ||
                            studentCourse.specialty !== programCourse.specializationIndex) {
                            studentCourse.semester = programCourse.semester;
                            studentCourse.units = programCourse.units;
                            studentCourse.ects = programCourse.ects;
                            studentCourse.coefficient = programCourse.coefficient;
                            studentCourse.programGroup = programCourse.studyProgramCourse.programGroup;
                            studentCourse.groupPercent = programCourse.studyProgramCourse.programGroupFactor;
                            studentCourse.courseType = programCourse.courseType;
                            studentCourse.specialty = programCourse.specializationIndex;
                            updateCourses.push(studentCourse);
                        }
                    }
                } else {
                    // course not found
                    //update calculateGrade, calculateUnits
                    if (event.target.excludeCourse === true) {
                        studentCourse.calculateGrade = false;
                        studentCourse.calculateUnits = false;
                        updateCourses.push(studentCourse);
                    }
                }

            });
            //save student courses
            await context.model('StudentCourse').save(updateCourses);

        }
    }
}


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    if (event.state === 4) {
        return callback();
    }
    // execute async method
    return afterSaveAsync(event).then((validationResult) => {
        event.target.validationResult = validationResult;
        if (validationResult && !validationResult.success) {
            return callback(validationResult);
        }
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    if (event.state === 4) {
        return callback();
    }
    // execute async method
    return beforeSaveAsync(event).then((validationResult) => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
