import {DataEventArgs, DataObjectState} from "@themost/data";
import {DataError, TraceUtils} from "@themost/common";

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return AfterCompleteTeachingEventListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

export class AfterCompleteTeachingEventListener {
    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {
        if (event.state === DataObjectState.Update && event.target.hasOwnProperty('eventStatus')) {
            if (event.previous == null) {
                throw new DataError('E_STATE', 'The previous state of the item cannot be determined', null, 'TeachingEvent')
            }
            const previousEventStatus = event.previous.eventStatus;
            const target = event.model.convert(event.target);
            const eventStatus = await target.property('eventStatus').getItem();
            if (eventStatus.id !== previousEventStatus.id) {
                const addAbsences = previousEventStatus.alternateName !== 'EventCompleted' && eventStatus.alternateName === 'EventCompleted';
                const context = event.model.context;
                const teachingEvent = await context.model('TeachingEvent').where('id').equal(event.target.id).expand(
                    {
                        'name': 'courseClass',
                        'options': {
                            '$expand': 'students'
                        }
                    },
                    {'name': 'eventStatus'},
                    {'name': 'superEvent'},
                    {'name': 'attendanceList'}
                ).silent().getItem();
                if (teachingEvent.hasOwnProperty('courseClass') && teachingEvent.courseClass.hasOwnProperty('students') && Array.isArray(teachingEvent.courseClass.students)) {
                    const absenceLimit = teachingEvent.courseClass.absenceLimit || 0;
                    let absenceClassStatus = null;
                    if (absenceLimit>0) {
                        // check if absence class status has been set only if courseClass has limit
                        if (teachingEvent.organizer) {
                            absenceClassStatus = await context.model('Department').where('id').equal(teachingEvent.organizer).select('organization/instituteConfiguration/absenceClassStatus as absenceClassStatus').silent().value();
                            if (absenceClassStatus) {
                                // ensure absenceClassStatus exists
                                const status = await context.model('StudentClassStatus').where('id').equal(absenceClassStatus).silent().getItem();
                                if (!status) {
                                    TraceUtils.warn('Student class status configured at instituteConfiguration does not exist.');
                                    absenceClassStatus = null;
                                }
                            }
                        }
                    }
                    let studentAbsences = [];
                    const students = teachingEvent.courseClass.students;
                    const attendanceList = teachingEvent.attendanceList;
                    for (const attendance of attendanceList) {
                        const student = students.find(x => {
                            return x.student === attendance.student;
                        });
                        if (student) {
                            const studentCourseClass = await context.model('StudentCourseClass')
                                .where('student').equal(student.student)
                                .and('courseClass').equal(teachingEvent.courseClass.id)
                                .select('id','absences','status')
                                .silent().getItem();
                            studentCourseClass.absences = studentCourseClass.absences || 0;
                            if (addAbsences) {
                                studentCourseClass.absences += !teachingEvent.presenceType ? teachingEvent.eventAttendanceCoefficient : 0;
                            } else {
                                studentCourseClass.absences -= !teachingEvent.presenceType ? teachingEvent.eventAttendanceCoefficient : 0;
                            }
                            // change also student course class status if absence limit is reached
                            if (absenceClassStatus != null && absenceLimit > 0 && studentCourseClass.absences > absenceLimit) {
                                studentCourseClass.status = absenceClassStatus;
                            }
                            studentAbsences.push({...studentCourseClass, $state: 2});
                        }
                    }
                    await context.model('StudentCourseClass').silent().save(studentAbsences)
                }
            }
        }
    }
}
