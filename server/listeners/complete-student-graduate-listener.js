/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
import {DataError} from "@themost/common";
import ActionStatusType from '../models/action-status-type-model';
import Student from '../models/student-model';
import * as _ from 'lodash';
import { GenericDataConflictError } from "../errors";

export function beforeSave(event, callback) {
    (async () => {
        const context = event.model.context;
        // validate student state (on insert or update)
        if (event.state === 1 || event.state === 2) {
            /**
             * @type {Student|DataObject|*}
             */
            const studentStatus = await context.model('Student').silent().where('id').equal(event.target.object)
                .silent()
                .select('studentStatus')
                .value();
            if (studentStatus == null) {
                throw new DataError('Action object cannot be found or is inaccessible');
            }
            // if student is not declared or graduated, throw error
            if (studentStatus.alternateName !== 'declared' && studentStatus.alternateName !== 'graduated' ) {
                throw new DataError('ERR_STATUS',
                    'Invalid student status. Student graduation action may execute upon a declared student only.'
                    , null,
                    'Student', 'studentStatus');
            }
        }
        /* Start graduation number calculation */
        if (event.state === 2 && event.target.hasOwnProperty('actionStatus')) {
            // check if student has an already calculated graduationNumber
            const student = event.previous.object;
            if (student == null) {
                throw new DataError('E_EMPTY_STUDENT', 'The student cannot be found in the previous object state.');
            }
            // if they do, do not recalculate
            if (student.hasOwnProperty('graduationNumber') && student.graduationNumber != null) {
                return;
            }
            // if the student's graduation is to be completed
            const previousActionStatus = event.previous && event.previous.actionStatus;
            if (previousActionStatus == null) {
                throw new DataError('E_PREVIOUS_STATUS', 'The previous action status of the object cannot be determined.');
            }
            // try to find the target action status (could be passed as id, alternateName etc)
            const actionStatus = await context.model('ActionStatusType').find(event.target.actionStatus).getItem();
            if (actionStatus == null) {
                throw new DataError('E_TARGET_STATUS', 'The target action status of the object cannot be found and cannot be empty.');
            }
            // note: CancelledActionStatus is checked to satisfy operations regarding UpdateStudentStatusAction
            if (!((previousActionStatus.alternateName === ActionStatusType.ActiveActionStatus
                || previousActionStatus.alternateName === ActionStatusType.CancelledActionStatus)
                && actionStatus.alternateName === ActionStatusType.CompletedActionStatus)) {
                    return;
                }
            // generate next graduation number and assign it to target
            // so it can be transfered to the student at the after-save state
            // but also saved at the StudentGraduateAction object
            if (student.department == null) {
                throw new DataError('E_EMPTY_DEPARTMENT', 'The student\'s department cannot be empty at the previous state.');
            }
            // get graduationNumberIndex from LocalDepartment
            const graduationNumberIndex = await context.model('LocalDepartment')
                .where('id').equal(student.department)
                .select('graduationNumberIndex')
                .silent()
                .value();
            // only if index is not null (note: allow the value 0)
            if (graduationNumberIndex == null) {
                return;
            }
            // assign the next graduation number to target
            event.target.graduationNumber = graduationNumberIndex + 1;
            // and update the local department index
            const updateGraduationNumberIndex = {
                id: student.department,
                graduationNumberIndex: graduationNumberIndex + 1
            }
            // if the after-save listener fails, this update will be undone (rollback)
            await context.model('LocalDepartment').silent().save(updateGraduationNumberIndex);
        }
        /* End graduation number calculation */
    })().then( () => {
       return callback();
    }).catch( err => {
        return callback(err);
    });
}


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    (async () => {
        const context = event.model.context;
        let target;
        // get own properties
        const attributes = event.model.attributes.filter( attribute => {
            if (attribute.primary) {
                return false;
            }
            return attribute.model === event.model.name;
        }).map( attribute => {
            return attribute.name;
        });
        // on insert
        if (event.state === 1) {
            // get target object
            target = await context.model(event.model.name).where('id').equal(event.target.id).silent().getTypedItem();
            // validate completed state
            if (target.actionStatus.alternateName !== ActionStatusType.CompletedActionStatus) {
                return;
            }
        }
        // on update
        else if (event.state === 2) {
            // get previous state
            const previousActionStatus = event.previous.actionStatus;
            // get target object
            target = await context.model(event.model.name).where('id').equal(event.target.id).silent().getTypedItem();
            // if graduation action is cancelled, change student status to declared and add student declaration
            if (previousActionStatus.alternateName === ActionStatusType.CompletedActionStatus &&
                target.actionStatus.alternateName === ActionStatusType.CancelledActionStatus) {
                // student should be declared
                //check if studentDeclareAction exists and add studentDeclaration
                const declareAction = await context.model('StudentDeclareAction').where('initiator').equal(target.initiator)
                    .and('actionStatus/alternateName').equal(ActionStatusType.CompletedActionStatus).silent().getItem();
                if (declareAction)
                {
                    // add studentDeclaration and change studentStatus
                    const declareAttributes = context.model('StudentDeclareAction').attributes.filter( attribute => {
                        if (attribute.primary) {
                            return false;
                        }
                        return attribute.model === 'StudentDeclareAction';
                    }).map( attribute => {
                        return attribute.name;
                    });
                    const studentDeclaration = _.pick(declareAction, declareAttributes);
                    studentDeclaration.student = declareAction.object;
                    await context.model('StudentDeclaration').silent().save(studentDeclaration);
                    // assign id and studentStatus
                    const studentUpdate = {
                        id: studentDeclaration.student.id,
                        studentStatus: {
                            alternateName: 'declared'
                        },
                    };
                    // update student status
                    await context.model('Student').silent().save(studentUpdate);
                }
            }
                if (((previousActionStatus.alternateName === ActionStatusType.ActiveActionStatus || previousActionStatus.alternateName === ActionStatusType.CancelledActionStatus ) &&
                target.actionStatus.alternateName === ActionStatusType.CompletedActionStatus) === false) {
                return;
            }
        }
        else {
            return;
        }
        // get student declaration
        const studentDeclaration = await context.model('StudentDeclaration').where('student').equal(target.object).silent().getItem();
        if (studentDeclaration == null) {
            throw new GenericDataConflictError('E_DECLARATION', 'The student declaration for the declared student does not exist.');
        }
        // and assign data
        const student = {
            id: target.object.id,
            studentStatus: {
                alternateName: 'graduated'
            }
        };
        // transfer attributes
        student.declaredDate = studentDeclaration.declaredDate;
        student.graduationDate = studentDeclaration.graduationDate;
        student.graduationPeriod = studentDeclaration.declaredPeriod;
        student.graduationYear = studentDeclaration.declaredYear;
        student.graduationGrade = studentDeclaration.graduationGrade;
        student.graduationGradeAdjusted =
            studentDeclaration.graduationGradeAdjusted;
        student.graduationGradeScale = studentDeclaration.graduationGradeScale;
        student.graduationGradeWrittenInWords =
            studentDeclaration.graduationGradeWrittenInWords;
        if (target.graduationNumber != null && target.object.hasOwnProperty('graduationNumber') && target.object.graduationNumber == null) {
            // transfer, also, graduationNumber from target object (StudentGraduateAction)
            student.graduationNumber = target.graduationNumber;
        }
        // get graduationEvent from initiator
        const request = await context.model('GraduationRequestAction')
            .where('id').equal(target.initiator).flatten().getItem();
        if (request && request.graduationEvent)
        { // assign  graduation event
            Object.assign(student, {
                graduationEvent:
                    {
                        "id": request.graduationEvent
                    }
            });
        }
        // do update
        await context.model('Student').silent().save(student);
        // delete student declaration
        await context.model('StudentDeclaration').silent().remove(studentDeclaration);

    })().then( () => {
        return callback();
    }).catch( err => {
        return callback(err);
    });
}
