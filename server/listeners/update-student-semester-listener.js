import {DataError} from "@themost/common";

/**
 * @param {DataEventArgs} event
 * @returns {Promise<void>}
 */
async function afterSaveAsync(event) {
    if (event.state === 1) {
        /**
         * get a snapshot of saved student period registration
         * @type {StudentPeriodRegistration}
         */
        const target = await event.model.where('id').equal(event.target.id)
            .select('student', 'semester', 'registrationYear', 'registrationPeriod')
            .flatten()
            .silent()
            .getTypedItem();
        if (target == null) {
            throw new DataError('EFOUND', 'Student period registration cannot be found', null, 'StudentPeriodRegistration');
        }
        // check if this registration is the last registration
        const isLastRegistration = await target.isLast();
        if (isLastRegistration) {
            // update student semester silently
            let student = {
                id: target.student,
                semester: target.semester
            };
            await event.model.context.model('Student').silent().save(student);
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch( err => {
        return callback(err);
    });
}
