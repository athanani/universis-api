import {PercentileRanking} from "@universis/percentile-ranking";
import {HttpServerError, TraceUtils} from "@themost/common";

/**
 *
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event){
    if (event?.target?.eventStatus?.alternateName === "EventCompleted" && event?.previous?.eventStatus?.alternateName !== "EventCompleted"){
        const context = event?.model?.context;
        const graduationEvent = await context.model('GraduationEvent').where('id').equal(event?.target?.id).expand({
            "name": "organizer",
            "options": {
                "$expand": "organization($expand=instituteConfiguration)"
            }
        }).getTypedItem();
        // get student requests made for the graduation event
        const studentRequests = await context.model('GraduationRequestActions')
            .where('graduationEvent').equal(graduationEvent?.id)
            .and('actionStatus/alternateName').equal('CompletedActionStatus')
            .and('student/graduationGrade').notEqual(null)
            .expand('student')
            .getTypedItems();
        let students = studentRequests.map(x => x.student);
        const graduationStudents = [...students];
        const graduationYears = students.map(x => x.graduationYear.id ?? x.graduationYear);
        let graduationYear = graduationYears.every( (val, i, arr) => val === arr[0] ) ? [graduationYears[0]] : graduationYears;
        let instituteConfiguration = graduationEvent?.organizer?.organization?.instituteConfiguration;
        // get students that have graduated in the same graduation year but don't have a student request
        let studentsWithoutRequests = await context.model('Students')
            .where('studentStatus/alternateName').equal('graduated')
            .and('graduationYear').in(graduationYear)
            .and('department').equal(graduationEvent?.organizer?.id ?? graduationEvent?.organizer)
            .and('id').notIn(students.map(x => x.id))
            .getItems();
        students = [...students, ...studentsWithoutRequests]
        // if the threshold isn't satisfied get graduated students from the previous academic year
        if(Array.isArray(graduationStudents) && graduationStudents.length < instituteConfiguration?.percentileRankItemThreshold) {
            let graduationEvents = await context.model('GraduationEvents')
                .where('organizer').equal(graduationEvent.organizer)
                .and('id').notEqual(graduationEvent?.id)
                .and('graduationYear').lowerOrEqual(graduationEvent?.graduationYear?.id ?? graduationEvent?.graduationYear)
                .orderByDescending('graduationYear').thenByDescending('graduationPeriod')
                .getTypedItems();
            for (const [eventIndex, event] of graduationEvents.entries()) {
                const requests = await context.model('GraduationRequestActions')
                    .where('graduationEvent').equal(event.id)
                    .and('actionStatus/alternateName').equal('CompletedActionStatus')
                    .and('student/graduationGrade').notEqual(null)
                    .expand('student')
                    .getItems();
                let studentsWithRequest = requests.map(x=> x?.student?.id ?? x?.student)
                students = [...students, ...requests.map(x=> x.student), ...(await context.model('Students')
                    .where('studentStatus/alternateName').equal('graduated')
                    .and('graduationYear').equal(event?.graduationYear?.id ?? event?.graduationYear)
                    .and('department').equal(graduationEvent?.organizer?.id ?? graduationEvent?.organizer)
                    .and('id').notIn(studentsWithRequest)
                    .getItems())];
                let otherGraduationRequestsInThatYear = graduationEvents.filter((item, index) => item.graduationYear.id === event.graduationYear.id && item.id !== event.id && index > eventIndex ).length
                if(students.length >= instituteConfiguration?.percentileRankItemThreshold && !otherGraduationRequestsInThatYear) {
                    break;
                }
            }
        }
        // filter students so that each student is only included once
        students = students.filter((value, index, self) => self.findIndex((m) => m.id === value.id) === index);
        let grades = students.map(x => {return {
            grade: Math.round(x.graduationGrade*10), student: x.id, percentileRank: x.graduationRankByGraduationYear
        }});

        switch (instituteConfiguration?.percentileRankMethod) {
            case 1:
                PercentileRanking.simplePercentileCalculation(grades);
                break;
            case 2:
                PercentileRanking.complexPercentileCalculation(grades);
                break;
            default:
                throw new HttpServerError("Percentile rank method not implemented yet.", "The percentile rank your institute has selected is not yet implemented.");
        }
        let res = [];
        // update only students in the current graduation event
        const filteredStudents = grades.filter(x=> graduationStudents.map(student=> student.id).includes(x.student));
        for (const item of filteredStudents){
            let {id, percentileRank} = item;
            let cc = students.find(x=> x.id === item.student)
            res.push({...cc,...id, graduationRankByGraduationYear: parseFloat(percentileRank.toFixed(4))});
        }
        try{
            return context.model('Students').silent().save(res);
        } catch (e) {
            TraceUtils.error(e);
        }
    }

}

export function afterSave(event, callback){
    afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
