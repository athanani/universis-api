import ActionStatusType from '../models/action-status-type-model';
import { DataObjectState } from '@themost/data';
import { DataConflictError } from '../errors';
import { DataError } from '@themost/common';
import * as _ from 'lodash';

/**
 * @async
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
	if (event.state !== DataObjectState.Insert) {
		return;
	}
	const context = event.model.context;
	// validate user existance
	const userExists = await context
		.model('User')
		.where('name')
		.equal(event.target && event.target.name)
		.select('id')
		.silent()
		.count();
	if (userExists) {
		throw new DataConflictError(`User ${event.target.name} already exists.`);
	}
}

/**
 * @async
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
	if (event.state !== DataObjectState.Update) {
		return;
	}
	const context = event.model.context;
	const target = event.model.convert(event.target);
	// get action status
	const actionStatus = await target.property('actionStatus').getItem();
	// if action status is other that completed
	if (actionStatus.alternateName !== ActionStatusType.CompletedActionStatus) {
		// exit
		return;
	}
	// get previous action status
	const previousActionStatus = event.previous && event.previous.actionStatus;
	if (previousActionStatus == null) {
		throw new DataError(
			'E_STATUS',
			'Previous status cannot be empty at this context.'
		);
	}
	// validate previous status
	if (previousActionStatus.alternateName !== 'ActiveActionStatus') {
		// throw error for invalid previous action status
		throw new DataError(
			'E_STATE',
			'Invalid action state. The action cannot be completed due to its previous state.'
		);
	}
	// get all shared user attributes
	// between UserReference and UserRegisterAction
	const UserReference = context.model('UserReference');
	const UserRegisterAction = context.model('UserRegisterAction');
	if (!UserReference || !UserRegisterAction) {
		return;
	}
	const userAttributes = UserReference.fields
		.filter((userAttribute) => {
			return (
				!userAttribute.primary &&
				userAttribute.model === 'UserReference' &&
				UserRegisterAction.fields.find(
					(field) => field.name === userAttribute.name
				)
			);
		})
		.map((userAttribute) => userAttribute.name);
	// transfer them to a new UserReference
	const newUser = _.pick(target, userAttributes);
	// and save silently
	await context.model('UserReference').silent().save(newUser);
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
	// execute async method
	return afterSaveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
	// execute async method
	return beforeSaveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}
