/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */

import {HttpNotFoundError} from "@themost/common";
import {GenericDataConflictError} from "../errors";

export function beforeSave(event, callback) {
    return RequestAttachmentEventListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return callback();
}

class RequestAttachmentEventListener {

    static async beforeSaveAsync(event) {
        /**
         * @type {DataContext|*}
         */
        const context = event.model.context;
        if (event.state !== 4) {
            // check if attachment type is defined at attachmentConfigurations
            if (event.target.object) {
                // get studentRequestAction
                let studentRequest = await context.model('StudentRequestAction')
                    .where('id').equal(event.target.object)
                    .expand('attachments').getItem();
                // load student request from original model
                if (studentRequest.additionalType !== 'StudentRequestAction') {
                    studentRequest = await context.model(studentRequest.additionalType)
                        .where('id').equal(event.target.object)
                        .expand('attachments').getItem();
                }
                if (!studentRequest) {
                    throw new HttpNotFoundError('E_STUDENT_REQUEST_NOT_FOUND', context.__('Student request action cannot be found'), 'StudentRequestActionAttachments');
                }
                const attachment = await context.model('Attachment').where('id').equal(event.target.value).flatten().silent().getItem();
                if (studentRequest.attachmentTypes) {

                    // check if supplied attachmentType is allowed
                    const attachmentType = studentRequest.attachmentTypes.find(x => {
                        return x.attachmentType === attachment.attachmentType;
                    });
                    if (attachmentType) {
                        if (attachmentType.numberOfAttachments > 0) {
                            // check if request has reached numberOfAttachments for the specified attachmentType
                            const countOfAttachments = (studentRequest.attachments || []).filter(x => {
                                return x.attachmentType === attachmentType.attachmentType
                            }).length;
                            if ((countOfAttachments + 1) > attachmentType.numberOfAttachments) {
                                throw new GenericDataConflictError('ER_ATTACHMENTS_REACHED', context.__('Number of allowed attachments for specified attachment type has been reached'), null, 'StudentRequestActionAttachments');
                            }
                        }
                    } else {
                        throw new GenericDataConflictError('E_ATTACHMENT_TYPE_NOT_ALLOWED', context.__('Attachment type is not allowed'), null, 'StudentRequestActionAttachments');
                    }

                } else {
                    throw new GenericDataConflictError('E_ATTACHMENT_TYPE_NOT_ALLOWED', context.__('Attachment type is not allowed'), null, 'StudentRequestActionAttachments');
                }

            } else {
                // attachment type should be supplied
                throw new GenericDataConflictError('E_ATTACHMENT_TYPE_MISSING', context.__('AttachmentType is missing'), null, 'StudentRequestActionAttachments');
            }
        }
    }

    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {

    }
}
