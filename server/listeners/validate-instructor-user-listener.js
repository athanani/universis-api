import { DataError } from "@themost/common";
import { DataObjectState } from "@themost/data";

/**
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
	if (event.state === DataObjectState.Update) {
		// get context
		const context = event.model.context,
			target = event.target,
			previous = event.previous;
		if (previous == null) {
			// previous state cannot be null on update
			throw new DataError(
				"The previous state of the object cannot be determined."
			);
		}
		// get target and previous user
		let targetUser = null;
		let previousUser = null;
		// Note: typeof null === 'object'
		if (target.user != null) {
			targetUser =
				typeof target.user === "object" ? target.user.id : target.user;
		}
		if (previous.user != null) {
			previousUser =
				typeof previous.user === "object" ? previous.user.id : previous.user;
		}
		// only if instructor user is modified
		if ((targetUser !== previousUser) && (targetUser != null)) {
			// try to find another instructor that is linked to the provided user
			const userAlreadyTaken = await context
				.model("Instructor")
				.where("id")
				.notEqual(target.id)
				.and("user")
				.equal(targetUser)
				.select("id", "familyName", "givenName")
				.silent()
				.getItem();
			// and validate
			if (userAlreadyTaken == null) {
				return;
			}
			throw new DataError(
				"E_USER_TAKEN",
				`The provided user ${targetUser} is already taken by Instructor [${userAlreadyTaken.id}]-${userAlreadyTaken.givenName} ${userAlreadyTaken.familyName}.`
			);
		}
	}
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
	return beforeSaveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}
