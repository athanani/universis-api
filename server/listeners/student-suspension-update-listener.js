/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
import {DataError, Args, DataNotFoundError} from "@themost/common";
import Student from '../models/student-model';
import * as _ from 'lodash';
import { DataConflictError } from "../errors";

export function beforeSave(event, callback) {
    (async () => {
        // validate student state (on insert or update)
        // todo:: move this action to an upper level
        if (event.state === 1) {
            let context = event.model.context;
            /**
             * @type {Student|DataObject|*}
             */
            const student = context.model('Student').convert(event.target.student);
            const isActive = await student.silent().isActive();
            if (!isActive) {
                throw new DataError('ERR_STATUS',
                    'Invalid student status. Student suspension may execute upon an active student only.'
                    , null,
                    'Student', 'studentStatus');
            }

            // get inscription year and period
           const studentObject = await context.model('Student').where('id').equal(student.id).select('id','inscriptionYear','inscriptionPeriod').silent().flatten().getItem();
            // check suspensionYear year and period
            const suspensionYear = _.isObject(event.target.suspensionYear) ? event.target.suspensionYear.id : event.target.suspensionYear;
            const suspensionPeriod  = _.isObject(event.target.suspensionPeriod) ? event.target.suspensionPeriod.id : event.target.suspensionPeriod;

            let checkYear = studentObject.inscriptionYear;
            let checkPeriod = studentObject.inscriptionPeriod
            const otherSuspension = await context.model('StudentSuspension')
                .where('student').equal(studentObject.id)
                .orderByDescending('reintegrationYear')
                .thenByDescending('reintegrationPeriod')
                .select('reintegrationYear', 'reintegrationPeriod')
                .flatten()
                .silent()
                .getItem();
            if (otherSuspension) {
                checkYear = otherSuspension.reintegrationYear;
                checkPeriod = otherSuspension.reintegrationPeriod;
            }
            if (suspensionYear < checkYear || (suspensionYear === checkYear  && suspensionPeriod < checkPeriod)) {
                throw new DataError('ERR_STATUS',
                    'Invalid suspension year. Student suspension year must be greater or equal than inscription year and greater than latest suspension reintgration year and period.'
                    , null,
                    'Student', 'studentStatus');
            }
        }
        if (event.state === 2 && event.target.hasOwnProperty('reintegrated')) {
            const previousReintegrated = event.previous && event.previous.reintegrated;
            if (previousReintegrated == null) {
                throw new DataError('E_PREVIOUS_REINTEGRATED', 'The previous reintegration state of the object cannot be determined.');
            }
            if (previousReintegrated !== 0 & event.target.reintegrated === 0 ) {
                // nullify reintegration attributes
                const reintegrationAttributes = ['reintegrationPeriod', 'reintegrationYear', 'reintegrationSemester', 
                    'reintegrationDate', 'reintegrationType' ,'reintegrationProtocolNumber'];
                reintegrationAttributes.forEach((attribute) => {
                    event.target[attribute] = null;
                });
            }

        }
    })().then( () => {
       return callback();
    }).catch( err => {
        return callback(err);
    });
}


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    (async () => {
        const context = event.model.context;
        let target;
        if (event.state === 2) {
            // get previous state
            const previousReintegrated = event.previous.reintegrated;
            // get target object
            target = await context.model(event.model.name).where('id').equal(event.target.id).silent().flatten().getTypedItem();
            const student = context.model('Student').convert(event.target.student);
            const suspensionYear =  target.suspensionYear;
            const suspensionPeriod  = target.suspensionPeriod;
            if (previousReintegrated === 0 && target.reintegrated === 1) {

                const reintegrationYear = target.reintegrationYear;
                const reintegrationPeriod  = target.reintegrationPeriod;
                if (_.isNil(reintegrationYear) || _.isNil(reintegrationPeriod) || _.isNil(event.target.reintegrationSemester)) {
                    throw new DataError('ERR_STATUS',
                        'Invalid data. Student reintegration fields should be set'
                        , null,
                        'StudentSuspension', 'reintegrationYear');
                }
                // check reintegration year and period
               if (reintegrationYear<suspensionYear || (reintegrationYear===suspensionYear  && reintegrationPeriod<suspensionPeriod)) {
                    throw new DataError('ERR_STATUS',
                        'Invalid reintegration year. Student reintegration year must be greater or equal than suspension year.'
                        , null,
                        'Student', 'studentStatus');
                }
                // assign id and studentStatus
                student.studentStatus = {
                    alternateName: 'active'
                };
                student.semester = event.target.reintegrationSemester;
                // update student status
                await context.model('Student').silent().save(student);
            } else if (previousReintegrated !== 0 && target.reintegrated === 0) {
                const object = event.model.convert(event.target);
                // validate that this is the last suspension
                const isLastSuspension = await object.isLastSuspension();
                if (!isLastSuspension) {
                    throw new DataConflictError(context.__('Reintegration cannot be reverted because this is not the last suspension of the student.'));   
                }
                // calculate student semester based on suspension year/period
                const studentSemester = await new Promise((resolve, reject) => {
                    return student.inferSemester(suspensionYear, suspensionPeriod, function (err, value) {
                        if (err) {
                            return reject(err);
                        }
                        try {
                            // validate semester type
                            Args.notNumber(value, 'Student semester');
                            // validate semester value
                            Args.check(value > 0, 'Student semester must be greater than zero');
                            // return calculated semester
                            return resolve(value);
                        } catch (err) {
                            return reject(err);
                        }
                    });
                });
                // assign semester and suspended status to student
                const revertStudentReintegration = {
                    id: target.student,
                    studentStatus: {
                        alternateName: 'suspended'
                    },
                    semester: studentSemester
                };
                // and update
                await context.model('Student').silent().save(revertStudentReintegration);
            }
        } else {
            return;
        }


    })().then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

export function beforeRemove(event, callback) {
    (async () => {
        const context = event.model.context;
        const target = event.target;
        // first, validate reintegrated attribute
        const studentSuspension = await context.model('StudentSuspension')
            .where('id')
            .equal(target.id)
            .select('id', 'reintegrated', 'student', 'student/studentStatus/alternateName as studentStatus')
            .getTypedItem();
        if (studentSuspension == null) {
            throw new DataNotFoundError(
                'The specified student suspension cannot be found or is inaccessible'
            );
        }
        if (studentSuspension.reintegrated) {
            throw new DataConflictError(
                context.__(
                    'The student suspension cannot be deleted because the student has been reintegrated.'
                )
            );
        }
        // validate student status
        if (studentSuspension.studentStatus !== 'suspended') {
            throw new DataConflictError(
                context.__(
                    'The student suspension cannot be deleted because the student is not suspended.'
                )
            );
        }
        // then, validate that this is the last suspension
        const isLastSuspension = await studentSuspension.isLastSuspension();
        if (!isLastSuspension) {
            throw new DataConflictError(
                context.__(
                    'The student suspension cannot be deleted because it is not the last one.'
                )
            );
        }
    })().then( () => {
       return callback();
    }).catch( err => {
        return callback(err);
    });
}


export function afterRemove(event, callback) {
    (async () => {
        const context = event.model.context;
        const previous = event.previous;
        if (previous == null) {
            throw new DataError('E_PREVIOUS', 'The previous state of the object cannot be determined', null, 'StudentSuspension');
        }
        // get current year and period
        const studentDepartment = await context.model('Student')
            .where('id').equal(previous.student)
            .select('department/currentYear as currentYear', 'department/currentPeriod as currentPeriod')
            .silent()
            .getItem();
        if (!(studentDepartment && studentDepartment.currentYear && studentDepartment.currentPeriod)) {
            throw new DataConflictError(`The current year and/or current period of the student's department are empty. Cannot calculate student's semester.`);
        }
        const student = context.model('Student').convert(previous.student);
        // calculate student semester based on current year/period
        const studentSemester = await new Promise((resolve, reject) => {
            return student.inferSemester(studentDepartment.currentYear, studentDepartment.currentPeriod, function (err, value) {
                if (err) {
                    return reject(err);
                }
                try {
                    // validate semester type
                    Args.notNumber(value, 'Student semester');
                    // validate semester value
                    Args.check(value > 0, 'Student semester must be greater than zero');
                    // return calculated semester
                    return resolve(value);
                } catch (err) {
                    return reject(err);
                }
            });
        });
        const activeStudentStatus = {
            alternateName: 'active'
        };
        // create an UpdateStudentStatusAction object before updating student
        // to keep track of previous/target statuses
        const updateStudentStatusAction = {
            object: previous.student,
            studentStatus: activeStudentStatus
        };
        await context.model('UpdateStudentStatusAction').silent().save(updateStudentStatusAction);
        // assign semester and active status to student
        const updateStudent = {
            id: previous.student,
            semester: studentSemester,
            studentStatus: activeStudentStatus
        };
        // and finally update student
        await context.model('Student').silent().save(updateStudent);

    })().then( () => {
       return callback();
    }).catch( err => {
        return callback(err);
    });
}
