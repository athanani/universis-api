import {DataError} from "@themost/common";

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return StudentThesisListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterRemove(event, callback) {
    return StudentThesisListener.afterRemoveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return StudentThesisListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

class StudentThesisListener {

    static async beforeSaveAsync(event) {
    }

    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {
        const context = event.model.context;
        // calculate final grade of thesis
        const previous = event.previous;
        const thesisResult = await context.model('StudentThesisResult').where('id').equal(event.target.id).flatten().getItem();

        if ((previous && previous.grade != event.target.grade) || (event.state === 1)) {
            /**
             @type StudentThesis
             **/
            const studentThesis = await context.model('StudentThesis').where('student').equal(thesisResult.student)
                .and('thesis').equal(thesisResult.thesis).getTypedItem();
            //calculate grade
            const grade = await studentThesis.calculateGrade();
            // save student Thesis
            studentThesis.grade = grade;
            await context.model('StudentThesis').save(studentThesis);
        }

    }
    static async afterRemoveAsync(event) {

    }
}
