import {DataEventArgs} from "@themost/data";
import {DataNotFoundError} from "@themost/common";


/**
 * @param {DataEventArgs} event
 * @param {function(Error=)} callback
 */
export function beforeSave(event, callback) {
    if (event.state === 1 || event.state === 2) {
        if(event.state === 2 && !event.previous) {
            return callback(new DataNotFoundError(event.model.context.__('The previous state of exam period cannot be determined.')));
        }
        if(event.target.hasOwnProperty('academicPeriods') && Array.isArray(event.target.academicPeriods)){
            event.target.periods = event.target.academicPeriods.map(x => x.id ?? x).reduce((a, b) => a + b, 0) ?? event.previous.periods ?? 0;
        }
        if(event.hasOwnProperty('previous') && event.previous.hasOwnProperty('academicPeriods') && event.previous.academicPeriods && event.previous.academicPeriods.length) {
            if(event.target.hasOwnProperty('academicPeriods') && Array.isArray(event.target.academicPeriods)) {
                event.previous.academicPeriods.forEach(period => {
                    if (!event.target.academicPeriods.map(x => x.id).includes(period.id)) {
                        event.target.academicPeriods = [...event.target.academicPeriods, typeof period === 'object' ? {
                            ...period,
                            '$state': 4
                        } : {'id': period, '$state': 4}];
                    }
                });
            }
        }
    }
    callback();
}
