import app from '../app';
import {assert} from 'chai';

class CancelError extends Error {
   //
}

describe('infer student semester', () => {

    /**
     * @type {ExpressDataContext}
     */
    let context;
    before(done => {
       context = app.get('ExpressDataApplication').createContext();
        context.user = {
          name: 'user_20128093@example.com'
        };
       return done();
    });

    after(done => {
        if (context) {
            return context.finalize(()=> {
               return done();
            });
        }
        return done();
    });

    it('should get student period registration', async ()=> {
        /**
         * @type {StudentPeriodRegistration|*}
         */
        let studentRegistration = await context.model('StudentPeriodRegistration')
            .where('student/user/name').equal(context.user.name)
            .orderByDescending('registrationYear')
            .thenByDescending('registrationPeriod')
            .silent()
            .getTypedItem();
        assert.isObject(studentRegistration);
        // calculate student semester
        let semester = await studentRegistration.inferStudentSemester();
        assert.isNumber(semester);
        assert.equal(studentRegistration.semester, semester);
    });

    it('should throw error for invalid data', async ()=> {
        /**
         * @type {StudentPeriodRegistration|*}
         */
        let studentRegistration = await context.model('StudentPeriodRegistration')
            .where('student/user/name').equal(context.user.name)
            .orderByDescending('registrationYear')
            .thenByDescending('registrationPeriod')
            .silent()
            .getTypedItem();
        assert.isObject(studentRegistration);
        // set registration year to null
        studentRegistration.registrationYear = null;
        try {
            // calculate student semester
            await studentRegistration.inferStudentSemester();
        }
        catch(err) {
            return assert.instanceOf(err, TypeError);
        }
        assert.isOk(false);
    });

    it('should check if student registration is the last registration', async ()=> {
        /**
         * @type {StudentPeriodRegistration|*}
         */
        let studentRegistration = await context.model('StudentPeriodRegistration')
            .where('student/user/name').equal(context.user.name)
            .orderByDescending('registrationYear')
            .thenByDescending('registrationPeriod')
            .silent()
            .getTypedItem();
        assert.isObject(studentRegistration);
        // calculate student semester
        let isLast = await studentRegistration.isLast();
        assert.isBoolean(isLast);
        assert.isOk(isLast);
    });

    it('should check if student registration is not the last registration', async ()=> {
        /**
         * @type {StudentPeriodRegistration|*}
         */
        let studentRegistrations = await context.model('StudentPeriodRegistration')
            .where('student/user/name').equal(context.user.name)
            .silent()
            .orderByDescending('registrationYear')
            .thenByDescending('registrationPeriod')
            .getTypedItems();
        assert.isArray(studentRegistrations);
        // check last registration
        let isLast = await studentRegistrations[0].isLast();
        assert.isOk(isLast);
        isLast = await studentRegistrations[1].isLast();
        assert.isNotOk(isLast);
        isLast = await studentRegistrations[2].isLast();
        assert.isNotOk(isLast);
    });

    it('should add student registration', async() => {
        /**
         *
         * @type {Student|*}
         */
        const student = await context.model('Student').where('user/name').equal('user_20141825@example.com')
            .expand('department')
            .silent()
            .getTypedItem();
        assert.isObject(student);
        /**
         * @type {StudentPeriodRegistration}
         */
        let registration = await student.getLastPeriodRegistration();
        // execute (and rollback)
        return await new Promise((resolve, reject)=> {
            return context.db.executeInTransaction((done)=> {
                if (registration == null) {
                    /**
                     * @type {DataObject|Array|*}
                     */
                    registration = context.model('StudentPeriodRegistration').convert({
                       student: student.id,
                       registrationYear: student.department.currentYear,
                       registrationPeriod: student.department.currentPeriod
                    });
                    // save (in unattended mode)
                    return context.unattended((cb) => {
                        return registration.save(context, (err) => {
                            if (err) {
                                return cb(err);
                            }
                            // check validation result
                            if (registration.validationResult && !registration.validationResult.success) {
                                return cb(registration.validationResult);
                            }

                            return cb(new CancelError());
                        });
                    }, err => {
                        return done(err);
                    });
                }
                return done();
            }, (err)=> {
                if (err instanceof CancelError) {
                    return resolve();
                }
                return reject(err);
            });
        });


    });

});

