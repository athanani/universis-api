{
  "$schema": "https://themost-framework.github.io/themost/models/2018/2/schema.json",
  "@id": "https://universis.io/schemas/Course",
  "name": "Course",
  "title": "Μάθημα",
  "description": "Αντικείμενο μίας θεματικής περιοχής διδασκαλίας",
  "hidden": false,
  "sealed": false,
  "version": "0.2",
  "classPath": "./models/course-model",
  "caching": "always",
  "source": "CourseBase",
  "view": "CourseData",
  "category": "main",
  "fields": [
    {
      "name": "id",
      "title": "Κωδικός μαθήματος",
      "description": "Μοναδικός κωδικός μαθήματος",
      "type": "Text",
      "nullable": false,
      "primary": true
    },
    {
      "name": "displayCode",
      "title": "Κωδικός εμφάνισης μαθήματος",
      "description": "Αποτελεί τον κωδικό με τον οποίο το μάθημα εμφανίζεται σε αναλυτικές βαθμολογίες, διαδικτυακούς τόπους κλπ. ",
      "type": "Text",
      "nullable": false,
      "size": 15
    },
    {
      "name": "department",
      "title": "Τμήμα",
      "description": "Πρόκειται για το τμήμα του οποίου η Γενική Συνέλευση είναι υπεύθυνη για τον ορισμό της ανάθεσης διδασκαλίας του μαθήματος.",
      "type": "Department",
      "nullable": false,
      "editable": false
    },
    {
      "name": "courseArea",
      "title": "Γνωστικό αντικείμενο",
      "description": "Κωδικός γνωστικού αντικειμένου",
      "type": "CourseArea",
      "nullable": true
    },
    {
      "name": "name",
      "title": "Τίτλος",
      "description": "Ο τίτλος του μαθήματος",
      "type": "Text",
      "nullable": false,
      "size": 150
    },
    {
      "name": "subtitle",
      "title": "Υπότιτλος",
      "description": "Ο υπότιτλος του μαθήματος",
      "type": "Text",
      "nullable": true,
      "size": 255
    },
    {
      "name": "isEnabled",
      "title": "Ενεργό (ΝΑΙ/ΟΧΙ)",
      "description": "Αν το μάθημα προσφέρεται ή έχει καταργηθεί",
      "type": "Boolean",
      "calculation": "javascript:return this.convertStrictBoolean(this.target,'isEnabled');",
      "nullable": false,
      "value": "javascript:return 1;"
    },
    {
      "name": "isShared",
      "title": "Κοινό μάθημα (ΝΑΙ/ΟΧΙ)",
      "description": "Πρόκειται για μαθήματα που προσφέρονται και σε άλλα προγράμματα σπουδών άλλων τμημάτων συνήθως ως μαθήματα τύπου ελεύθερης επιλογής. ",
      "calculation": "javascript:return this.convertStrictBoolean(this.target,'isShared');",
      "type": "Boolean",
      "nullable": false,
      "value": "javascript:return 0;"
    },
    {
      "name": "gradeScale",
      "title": "Κλίμακα βαθμολογίας",
      "description": "Η πιο διαδεδομένη βαθμολογική κλίμακα είναι η δεκαδική (από 0 έως 10 με βάση το 5), ωστόσο κάποια χρησιμοποιούν και άλλες βαθμολογικές κλίμακες (πχ. Λατινικά γράμματα, Επιτυχώς/Ανεπιτυχώς, δεκαδική από 0 έως 10 με βάση το 6 κ.α.) ",
      "type": "GradeScale",
      "expandable": true
    },
    {
      "name": "instructor",
      "title": "Υπεύθυνος μαθήματος",
      "description": "Ο επιστημονικά υπεύθυνος του μαθήματος",
      "type": "Instructor"
    },
    {
      "name": "isCalculatedInScholarship",
      "title": "Υπολογίζεται στις υποτροφίες (ΝΑΙ/ΟΧΙ)",
      "description": "Αν το μάθημα υπολογίζεται στις υποτροφίες (ΝΑΙ/ΟΧΙ)",
      "type": "Boolean",
      "calculation": "javascript:return this.convertStrictBoolean(this.target,'isCalculatedInScholarship');",
      "value": "javascript:return 1;"
    },
    {
      "name": "units",
      "title": "Διδακτικές μονάδες",
      "description": "Οι διδακτικές μονάδες του μαθήματος",
      "type": "Number"
    },
    {
      "name": "courseUrl",
      "title": "Ηλεκτρονική διεύθυνση",
      "description": "Η ηλεκτρονική διεύθυνση με περισσότερες πληροφορίες για το μάθημα",
      "type": "Text",
      "nullable": true,
      "size": 150
    },
    {
      "name": "notes",
      "title": "Σημειώσεις",
      "description": "Σημειώσεις για το μάθημα",
      "type": "Note",
      "nullable": true,
      "many": false
    },
    {
      "name": "replacedByCourse",
      "title": "Αντικαταστάθηκε από το μάθημα",
      "description": "Στις περιπτώσεις κατά τις οποίες το μάθημα αντικαταστάθηκε από κάποιο άλλο, στο πεδίο αυτό καταχωρείται ο κωδικός του νέου μαθήματος που το αντικατέστησε",
      "type": "Course",
      "nullable": true
    },
    {
      "name": "replacedCourse",
      "title": "Οι κωδικοί των μαθημάτων που αντικατέστησε",
      "description": "Οι κωδικοί των μαθημάτων που αντικατέστησε διαχωριζόμενοι με κόμμα",
      "type": "Text",
      "nullable": true,
      "size": 255
    },
    {
      "name": "maxNumberOfRemarking",
      "title": "Μέγιστος αριθμός αναβαθμολογήσεων",
      "description": "Ο μέγιστος αριθμός των αναβαθμολογήσεων που επιτρέπεται στο μάθημα",
      "type": "Number",
      "value": "javascript:return 4;"
    },
    {
      "name": "parentCourse",
      "title": "Πατρικό μάθημα",
      "description": "Στις περιπτώσεις κατά τις οποίες το μάθημα αποτελεί μέρος άλλου μαθήματος, στο πεδίο αυτό καταχωρείται ο κωδικός του πατρικού του",
      "type": "Course",
      "mapping": {
        "parentModel": "Course",
        "childModel": "Course",
        "parentField": "id",
        "childField": "parentCourse",
        "cascade": "delete",
        "associationType": "association"

      }
    },
    {
      "name": "coursePartPercent",
      "title": "Ποσοστό συμμετοχής του βαθμού στο πατρικό μάθημα",
      "description": "Στις περιπτώσεις κατά τις οποίες το μάθημα αποτελεί μέρος άλλου μαθήματος, στο πεδίο αυτό καταχωρείται το ποσοστό συμμετοχής στον τελικό βαθμό του μαθήματος (από 0 έως 1)",
      "type": "Number",
      "nullable": true,
      "size": 4
    },
    {
      "name": "calculatedCoursePart",
      "title": "Υπολογίζεται στο βαθμό του πατρικού μαθήματος",
      "description": "Στις περιπτώσεις κατά τις οποίες το μάθημα αποτελεί μέρος άλλου μαθήματος δηλώνεται αν υπολογίζεται στο βαθμό του πατρικού μαθήματος",
      "type": "Boolean",
      "calculation": "javascript:return this.convertStrictBoolean(this.target,'calculatedCoursePart');"

    },
    {
      "name": "courseStructureType",
      "title": "Τύπος δομής μαθήματος",
      "description": "Ανάλογα με τη διδασκαλία και τον τρόπο βαθμολόγησης ενός μαθήματος γίνεται η κατηγοριοποίησή του βάσει του τύπου δομής. (πχ Απλό μάθημα , μικτό μάθημα, μέρος μαθήματος).",
      "type": "CourseStructureType",
      "value": "javascript:return 1;",
      "nullable": false,
      "readonly": true
    },
    {
      "name": "courseSector",
      "title": "Τομέας",
      "description": "Ο τομέας του τμήματος που ανήκει το μάθημα",
      "type": "CourseSector"
    },
    {
      "name": "courseCategory",
      "title": "Κατηγορία μαθήματος",
      "description": "Αναφέρεται στον τρόπο διδασκαλίας του μαθήματος (πχ. Εργαστηριακό, Διάλεξη κλπ) ",
      "type": "CourseCategory"
    },
    {
      "name": "ects",
      "title": "Μονάδες ECTS",
      "description": "Μονάδες ECTS",
      "type": "Number",
      "nullable": true
    },
    {
      "name": "isLocal",
      "title": "Ανήκει στο Ίδρυμα (ΝΑΙ/ΟΧΙ)",
      "description": "Ανήκει στο Ίδρυμα (ΝΑΙ/ΟΧΙ)",
      "type": "Boolean",
      "calculation": "javascript:return this.convertStrictBoolean(this.target,'isLocal');",
      "value": "javascript:return 1;"
    },
    {
      "name": "dateModified",
      "title": "Ημερομηνία τελευταίας τροποποίησης",
      "description": "Ημερομηνία τελευταίας τροποποίησης",
      "type": "DateTime",
      "nullable": false,
      "value": "javascript:return new Date();",
      "calculation": "javascript:return this.now();"
    },
    {
      "name": "calculatedInRegistration",
      "title": "Υπολογίζεται στους κανόνες δήλωσης",
      "description": "Υπολογίζεται στους κανόνες δήλωσης (ΝΑΙ/ΟΧΙ)",
      "type": "Boolean",
      "calculation": "javascript:return this.convertStrictBoolean(this.target,'calculatedInRegistration');",
      "value": "javascript:return 1;"
    },
    {
      "name": "courseParts",
      "title": "Μαθήματα",
      "description": "Το μέρη ενός μαθήματος.",
      "type": "Course",
      "many": true,
      "mapping": {
        "parentModel": "Course",
        "childModel": "Course",
        "parentField": "id",
        "childField": "parentCourse",
        "cascade": "delete",
        "associationType": "association"

      }
    },
    {
      "name": "locales",
      "type": "CourseLocale",
      "mapping": {
        "associationType": "association",
        "cascade": "delete",
        "parentModel": "Course",
        "parentField": "id",
        "childModel": "CourseLocale",
        "childField": "object"
      }
    },
    {
      "name": "locale",
      "type": "CourseLocale",
      "readonly": true,
      "editable": false,
      "many": true,
      "expandable": true,
      "multiplicity": "ZeroOrOne",
      "mapping": {
        "associationType": "association",
        "cascade": "delete",
        "parentModel": "Course",
        "parentField": "id",
        "childModel": "CourseLocale",
        "childField": "object",
        "options": {
          "$filter": "inLanguage eq lang()",
          "$first": true
        }
      }
    }
  ],
  "privileges": [
    {
      "mask": 15,
      "type": "global"
    },
    {
      "mask": 1,
      "type": "global",
      "account": "*"
    },
    {
      "mask": 15,
      "type": "global",
      "account": "Administrators"
    },
    {
      "mask": 15,
      "type": "self",
      "account": "Registrar",
      "filter": "department eq departments()"
    }
  ],
  "eventListeners": [
    {
      "type": "./listeners/data-localization"
    },
    {
      "type": "@themost/data/previous-state-listener"
    },
    {
      "type": "./listeners/course-listener"
    }
  ]
}
