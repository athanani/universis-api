{
    "$schema": "https://themost-framework.github.io/themost/models/2018/2/schema.json",
    "@id": "http://schema.org/CreativeWork",
    "name": "CreativeWork",
    "description": "The most generic kind of creative work, including books, movies, photographs, software programs, etc.",
    "title": "CreativeWork",
    "abstract": false,
    "sealed": false,
    "hidden": true,
    "implements": "Thing",
    "version": "1.0",
    "fields": [
        {
            "@id": "http://schema.org/about",
            "name": "about",
            "title": "about",
            "description": "The subject matter of the content.",
            "type": "Text"
        },
        {
            "@id": "http://schema.org/audience",
            "name": "audience",
            "title": "audience",
            "description": "An intended audience, i.e. a group for whom something was created.",
            "type": "Audience"
        },
        {
            "@id": "http://schema.org/isPartOf",
            "name": "isPartOf",
            "title": "isPartOf",
            "description": "Indicates an item or CreativeWork that this item, or CreativeWork (in some sense), is part of.",
            "type": "CreativeWork"
        },
        {
            "@id": "http://schema.org/video",
            "name": "video",
            "title": "video",
            "description": "An embedded video object.",
            "type": "URL"
        },
        {
            "@id": "http://schema.org/text",
            "name": "text",
            "title": "text",
            "description": "The textual content of this CreativeWork.",
            "type": "Note"
        },
        {
            "@id": "http://schema.org/expires",
            "name": "expires",
            "title": "expires",
            "description": "Date the content expires and is no longer useful or available. For example a VideoObject or NewsArticle whose availability or relevance is time-limited, or a ClaimReview fact check whose publisher wants to indicate that it may no longer be relevant (or helpful to highlight) after some date.",
            "type": "Date",
            "many": false
        },
        {
            "@id": "http://schema.org/contributor",
            "name": "contributor",
            "title": "contributor",
            "description": "A secondary contributor to the CreativeWork or Event.",
            "type": "Account"
        },
        {
            "@id": "http://schema.org/publisher",
            "name": "publisher",
            "title": "publisher",
            "description": "The publisher of the creative work.",
            "type": "Account"
        },
        {
            "@id": "http://schema.org/position",
            "name": "position",
            "title": "position",
            "description": "The position of an item in a series or sequence of items.",
            "type": "Integer"
        },
        {
            "@id": "http://schema.org/educationalUse",
            "name": "educationalUse",
            "title": "educationalUse",
            "description": "The purpose of a work in the context of education; for example, 'assignment', 'group work'.",
            "type": "Text"
        },
        {
            "@id": "http://schema.org/editor",
            "name": "editor",
            "title": "editor",
            "description": "Specifies the Person who edited the CreativeWork.",
            "type": "Account",
            "value": "javascript:return this.user();",
            "calculation": "javascript:return this.user();"
        },
        {
            "@id": "http://schema.org/discussionUrl",
            "name": "discussionUrl",
            "title": "discussionUrl",
            "description": "A link to the page containing the comments of the CreativeWork.",
            "type": "URL"
        },
        {
            "@id": "http://schema.org/award",
            "name": "award",
            "title": "award",
            "description": "An award won by or for this item.",
            "type": "Text"
        },
        {
            "@id": "http://schema.org/copyrightHolder",
            "name": "copyrightHolder",
            "title": "copyrightHolder",
            "description": "The party holding the legal copyright to the CreativeWork.",
            "type": "Account"
        },
        {
            "@id": "http://schema.org/copyrightYear",
            "name": "copyrightYear",
            "title": "copyrightYear",
            "description": "The year during which the claimed copyright for the CreativeWork was first asserted.",
            "type": "Number"
        },
        {
            "@id": "http://schema.org/commentCount",
            "name": "commentCount",
            "title": "commentCount",
            "description": "The number of comments this CreativeWork (e.g. Article, Question or Answer) has received. This is most applicable to works published in Web sites with commenting system; additional comments may exist elsewhere.",
            "type": "Integer"
        },
        {
            "@id": "http://schema.org/fileFormat",
            "name": "fileFormat",
            "title": "fileFormat",
            "description": "Media type, typically MIME format (see IANA site) of the content e.g. application/zip of a SoftwareApplication binary. In cases where a CreativeWork has several media type representations, 'encoding' can be used to indicate each MediaObject alongside particular fileFormat information. Unregistered or niche file formats can be indicated instead via the most appropriate URL, e.g. defining Web page or a Wikipedia entry.",
            "type": "Text"
        },
        {
            "@id": "http://schema.org/inLanguage",
            "name": "inLanguage",
            "title": "inLanguage",
            "description": "The language of the content or performance or used in an action. Please use one of the language codes from the IETF BCP 47 standard. See also availableLanguage.",
            "type": "Text"
        },
        {
            "@id": "http://schema.org/contentRating",
            "name": "contentRating",
            "title": "contentRating",
            "description": "Official rating of a piece of content—for example,'MPAA PG-13'.",
            "type": "Text"
        },
        {
            "@id": "http://schema.org/learningResourceType",
            "name": "learningResourceType",
            "title": "learningResourceType",
            "description": "The predominant type or kind characterizing the learning resource. For example, 'presentation', 'handout'.",
            "type": "Text"
        },
        {
            "@id": "http://schema.org/version",
            "name": "version",
            "title": "version",
            "description": "The version of the CreativeWork embodied by a specified resource.",
            "type": "Text"
        },
        {
            "@id": "http://schema.org/genre",
            "name": "genre",
            "title": "genre",
            "description": "Genre of the creative work, broadcast channel or group.",
            "type": "Text"
        },
        {
            "@id": "http://schema.org/keywords",
            "name": "keywords",
            "title": "keywords",
            "description": "Keywords or tags used to describe this content. Multiple entries in a keywords list are typically delimited by commas.",
            "type": "Text",
            "many": false
        },
        {
            "@id": "http://schema.org/author",
            "name": "author",
            "title": "author",
            "description": "The author of this content.",
            "type": "Account",
            "value": "javascript:return this.user();"
        },
        {
            "@id": "http://schema.org/thumbnailUrl",
            "name": "thumbnailUrl",
            "title": "thumbnailUrl",
            "description": "A thumbnail image relevant to the Thing.",
            "type": "URL"
        },
        {
            "@id": "http://schema.org/hasPart",
            "name": "hasPart",
            "title": "hasPart",
            "description": "Indicates an item or CreativeWork that is part of this item, or CreativeWork (in some sense).",
            "type": "CreativeWork",
            "many": true,
            "mapping": {
                "associationType": "association",
                "parentModel": "CreativeWork",
                "parentField": "id",
                "childModel": "CreativeWork",
                "childField": "isPartOf"
            }
        },
        {
            "@id": "http://schema.org/comment",
            "name": "comments",
            "title": "comments",
            "description": "Comments, typically from users.",
            "type": "Comment",
            "many": true,
            "mapping": {
                "associationType": "junction",
                "associationObjectField": "about",
                "associationValueField": "comment",
                "associationAdapter": "CreativeWorkComments",
                "parentModel": "CreativeWork",
                "parentField": "id",
                "childModel": "Comment",
                "childField": "id",
                "cascade": "delete"
            }
        },
        {
            "@id": "http://schema.org/encodingFormat",
            "name": "encodingFormat",
            "title": "encodingFormat",
            "description": "Media type typically expressed using a MIME format (see IANA site and MDN reference) e.g. application/zip for a SoftwareApplication binary, audio/mpeg for .mp3 etc.).In cases where a CreativeWork has several media type representations, encoding can be used to indicate each MediaObject alongside particular encodingFormat information.Unregistered or niche encoding and file formats can be indicated instead via the most appropriate URL, e.g. defining Web page or a Wikipedia/Wikidata entry.",
            "type": "Text"
        },
        {
            "@id": "http://schema.org/license",
            "name": "license",
            "title": "license",
            "description": "A license document that applies to this content, typically indicated by URL.",
            "type": "Text"
        },
        {
            "@id": "http://schema.org/creator",
            "name": "creator",
            "title": "creator",
            "description": "The creator/author of this CreativeWork. This is the same as the Author property for CreativeWork.",
            "type": "Account",
            "readonly": true,
            "value": "javascript:return this.user();"
        },
        {
            "@id": "http://schema.org/datePublished",
            "name": "datePublished",
            "title": "datePublished",
            "description": "Date of first broadcast/publication.",
            "type": "DateTime"
        },
        {
            "@id": "http://schema.org/isAccessibleForFree",
            "name": "isAccessibleForFree",
            "title": "isAccessibleForFree",
            "description": "A flag to signal that the item, event, or place is accessible for free.",
            "type": "Boolean"
        }
    ],
    "privileges": [
        {
            "mask": 15,
            "type": "global"
        },
        {
            "mask": 15,
            "type": "global",
            "account": "Administrators"
        }
    ]
}