import { ApplicationService } from '@themost/common';
import { DataObject, SchemaLoaderStrategy, ModelClassLoaderStrategy } from '@themost/data';

class Student extends DataObject {
    /**
     * 
     * @param {number} year 
     * @param {number} period 
     * @param {function(Error=,Number=)} callback 
     */
    inferSemester(year, period, callback) {
        void this.inferSemesterAsync(year, period).then(function(result) {
            return callback(null, result);
        }).catch(function(err) {
            return callback(err);
        });
    }

    /**
     * @param {number} year
     * @param {number} period
     * @returns {Promise<number>}
     */
    async inferSemesterAsync(year, period) {

        /**
         * @type {Student}
         */
        const student = await this.context.model('Student')
            .where('id').equal(this.id)
            .select('id', 'semester', 'inscriptionYear', 'inscriptionPeriod', 'inscriptionSemester', 'studentStatus/alternateName as studentStatus')
            .flatten()
            .getItem();
        let semester = student.semester;
        if (student.studentStatus === 'active') {
            // get last suspension if any
            const suspension = await this.context.model('StudentSuspension')
                .where('student').equal(this.id)
                .orderByDescending('reintegrationYear')
                .thenByDescending('reintegrationPeriod')
                .where('reintegrated').equal(true)
                .flatten()
                .getItem();
            if (suspension) {
                if (suspension.reintegrated) {
                    semester = (year - suspension.reintegrationYear) * 2 + (period - suspension.reintegrationPeriod) + suspension.reintegrationSemester;
                }
            } else {
                semester = (year - student.inscriptionYear) * 2 + (period - student.inscriptionPeriod) + student.inscriptionSemester;
            }
        }
        if (semester < 1) {
            semester = student.semester;
        }
        return semester;
    }

}

class StudentInferSemesterProvider extends ApplicationService {
    
    constructor(app) {
        super(app);
        this.apply();
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition('Student');
        // get model class
        const loader = this.getApplication().getConfiguration().getStrategy(ModelClassLoaderStrategy);
        const StudentBase = loader.resolve(model);
        // extend class
        Object.assign(StudentBase.prototype, {
            inferSemester: Student.prototype.inferSemester,
            inferSemesterAsync: Student.prototype.inferSemesterAsync
        });
    }
}

export {
    StudentInferSemesterProvider
}