import { TraceUtils, DataError, DataNotFoundError } from "@themost/common";
import { DataObjectState, DataPermissionEventListener, PermissionMask } from "@themost/data";
import { DataConflictError } from "../errors";
import { getMailer } from "@themost/mailer";
import { CreateExternalInstructorUserStrategy } from "./CreateExternalInstructorUserStrategy";
import {promisify} from "es6-promisify";

/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
	if (event.state !== DataObjectState.Insert) {
		return;
	}
	// get validator listener
	const validator = new DataPermissionEventListener();
	// noinspection JSUnresolvedFunction
	const validateAsync = promisify(validator.validate)
		.bind(validator);
	// validate Student/SendActivationMessage execute permission
	const validateEvent = {
		model: event.model.context.model('Instructor'),
		privilege: 'Instructor/CreateExternalUser',
		mask: PermissionMask.Execute,
		target: event.target && event.target.instructor,
		throwError: true
	}
	await validateAsync(validateEvent);
	const context = event.model.context;
	const instructorId =
		typeof event.target.instructor === "object"
			? event.target.instructor.id
			: event.target.instructor;
	// get instructor
	const instructor = await context
		.model("Instructor")
		.where("id")
		.equal(instructorId)
		.expand("user")
		.getItem();
	if (instructor == null) {
		throw new DataNotFoundError(
			"The specified instructor cannot be found or is inaccesible"
		);
	}
	if (instructor.user == null) {
		throw new DataError(
			"E_NOENT",
			"The instructor user has not yet been created or is inaccessible",
			null,
			"CreateInstructorUserAction"
		);
	}
	// get oauth2 client service
	const service = context
		.getApplication()
		.getService(function OAuth2ClientService() {});
	// validate createUser and getUser functions
	if (
		typeof service.createUser !== "function" ||
		typeof service.getUser !== "function"
	) {
		throw new DataConflictError(
			"The operation is not supported by the oauth2 client service."
		);
	}
	// get access token (for admin account)
	const adminAccount = service.settings.adminAccount;
	if (adminAccount == null) {
		throw new DataConflictError(
			"The operation cannot be completed due to invalid or missing configuration."
		);
	}
	// authorize user and get token
	const authorizeUser = await service.authorize(adminAccount);
	// check if user exists in oauth2 server
	let userExists = await service.getUser(instructor.user.name, authorizeUser);
	let index = 0;
	while (userExists) {
		index++;
		userExists = await service.getUser(
			instructor.user.name + index.toString(),
			authorizeUser
		);
	}
	// append the final index, if any
	if (index) {
		instructor.user.name += index.toString();
		// and also update local user
		const updateLocalUser = {
			id: instructor.user.id,
			name: instructor.user.name,
			$state: 2,
		};
		await context.model("User").silent().save(updateLocalUser);
	}
	// get activation code
	const activationCode = event.target.activationCode;
	if (activationCode == null) {
		throw new Error("Activation code is missing.");
	}
	// create user in oauth2 server
	await service.createUser(
		{
			name: instructor.user.name,
			description: `${instructor.givenName} ${instructor.familyName}`,
			alternateName: instructor.email,
			enabled: true,
			familyName: instructor.familyName,
			givenName: instructor.givenName,
			userCredentials: {
				userPassword: `{clear}${activationCode}`,
				temporary: true
			},
		},
		authorizeUser
	);
	try {
		// send notification for the newly created user
		const mailer = getMailer(context);
		const mailTemplate = await context
			.model("MailConfiguration")
			.where("target")
			.equal("CreateInstructorUserAction")
			.silent()
			.getItem();
		// if mail template is null do nothing and exit
		// otherwise send mail
		if (mailTemplate != null) {
			await new Promise((resolve) => {
				mailer
					.template(mailTemplate.template)
					.subject(mailTemplate.subject)
					.to(instructor.email)
					.send(
						{
							model: {
								instructor,
								action: event.target,
							},
						},
						(err) => {
							if (err) {
								try {
									TraceUtils.error(
										"CreateInstructorUserAction",
										"An error occurred while trying to send an email notification for account activation."
									);
									TraceUtils.error(
										"CreateInstructorUserAction",
										"Instructor",
										`${instructor.id}`,
										"Email Address",
										`${instructor.email || "empty"}`
									);
									TraceUtils.error(err);
								} catch (err1) {
									// do nothing
								}
								return resolve();
							}
							return resolve();
						}
					);
			});
		} else {
			TraceUtils.warn(
				"CreateInstructorUserAction",
				"A mail template for account activation notification is missing. User cannot be notified for this action."
			);
		}
	} catch (err) {
		TraceUtils.error(
			"CreateInstructorUserAction",
			"An error occurred while trying to send an email notification for account activation."
		);
		TraceUtils.error(err);
	}
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
	afterSaveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			TraceUtils.error(err);
			return callback(err);
		});
}

export class CreateExternalInstructorUser extends CreateExternalInstructorUserStrategy {
	constructor(app) {
		// install this listener as member of event listeners of CreateInstructorUserAction model
		super(app, "CreateInstructorUserAction", __filename);
	}
}
