import {ApplicationService, TraceUtils} from '@themost/common';


class UserProvisioningService extends ApplicationService {
    constructor(app) {
        super(app);
    }

    /*
       check if user is related to person based on uniqueIdentifier value and assign new username
    */
    async validateUser(context, info) {
        //
        // get uniqueIdentifier settings by scope
        try {
            // check settings section
            if (info.scope == null) {
                TraceUtils.error(`User provisioning service: Scope is not available`);
                return null;
            }
            const settings = (context.getConfiguration().getSourceAt('settings/universis/userProvisioning') || []).find(x => {
                return x.scope === info.scope;
            });
            if (!settings) {
                TraceUtils.error(`User provisioning service: Settings are not set for scope ${info.scope}`);
                return null;
            }
            const uniqueIdentifierAttribute = settings.userAttribute || 'uniqueIdentifier';
            const tokenAttributeName = settings.tokenAttribute || 'uniqueIdentifier';
            const tokenAttribute = info[tokenAttributeName];
            const scopes = info.scope.split(',').map(x => x.trim());
            if (scopes.indexOf('students')>-1 || scopes.indexOf('teachers')>-1) {
                const model = scopes.indexOf('students')>-1 ? 'Students' : 'Instructors';
                if (tokenAttribute == null) {
                    TraceUtils.error(`Missing attribute ${tokenAttributeName} from token info.`);
                    return null;
                }
                const count = await context.model(model)
                    .where(uniqueIdentifierAttribute).equal(tokenAttribute)
                    .select('id').silent()
                    .count();
                if (count !== 1) {
                    TraceUtils.log(`For username ${info.username} ${count} ${model} found with ${uniqueIdentifierAttribute} ${tokenAttribute}`);
                    return null;
                } else {
                    // object with uniqueIdentifier found
                    // get object user
                    const item = await context.model(model).where(uniqueIdentifierAttribute).equal(tokenAttribute).expand('user').silent().getItem();
                    if (item.user == null) {
                        TraceUtils.log(`${model} with ${uniqueIdentifierAttribute} ${tokenAttribute} is not related with user`);
                        return null;
                    } else {
                        // rename user
                        let user = {
                            id: item.user.id,
                            name: info.username
                        };
                        try {
                            await context.model('UserReference').silent().save(user);
                            // reload user
                            user = await context.model('User').where('name').equal(info.username).silent().getItem();
                            return user;
                        } catch (err) {
                            TraceUtils.error(`An error occurred while trying to rename user ${info.username} with ${uniqueIdentifierAttribute} ${tokenAttribute}`);
                            TraceUtils.error(err);
                        }
                    }
                }
            }
            // only if the unknown user comes with a single candidates scope
            if (scopes.indexOf('candidates') > -1 && scopes.length === 1) {
                // validate create guest user setting
                if (settings.createGuestUser !== true) {
                    return null;
                }
                // fetch candidates group
                const candidatesGroup = await context.model('Group')
                    .where('alternateName').equal('Candidates')
                    .select('id')
                    .silent()
                    .value();
                if (candidatesGroup == null) {
                    TraceUtils.error(`[UserProvisioningService]: An error occurred while trying to create guest user for ${info.username}. Candidates group is missing.`);
                    return null;
                }
                // get student scope settings
                const studentScopeSettings = (context.getConfiguration().getSourceAt('settings/universis/userProvisioning') || []).find(x => {
                    return x.scope === 'students';
                });
                // if they exist
                if (studentScopeSettings && studentScopeSettings.userAttribute && studentScopeSettings.tokenAttribute) {
                    // get unique identifier attribute name
                    const studentUniqueIdentifierAttribute = studentScopeSettings.userAttribute;
                    // get token attribute
                    const studentTokenAttributeName = studentScopeSettings.tokenAttribute;
                    const studentTokenAttribute = info[studentTokenAttributeName];
                    if (studentTokenAttribute) {
                        // try to find if the logged in user is matched with a student
                        // (e.g. a renamed user that has NOT been yet provisioned by the 'students' scope)
                        const count = await context.model('Student')
                            .where(studentUniqueIdentifierAttribute).equal(studentTokenAttribute)
                            .select('id')
                            .silent()
                            .count();
                        if (count > 0) {
                            TraceUtils.log(`[UserProvisioningService]: User ${info.username} has been matched to a student with ${studentUniqueIdentifierAttribute} ${studentTokenAttribute}. Aborting user creation.`);
                            // and exit
                            return null;
                        }
                    }
                }
                let fullName = info.username, email = info.email, userEnabled = -1;
                if (settings.useOAuthUserData === true) {
                    // get oauth2 client service
                    const service = context
                        .getApplication()
                        .getService(function OAuth2ClientService() {});
                    // validate getUser function
                    if (
                        typeof service.getUser !== 'function'
                    ) {
                        TraceUtils.error(`[UserProvisioningService]: Cannot fetch user data from OAuth2 server for ${info.username}. The operation is not supported by the OAuth2 client service.`);
                        return null;
                    }
                    // get access token (for admin account)
                    const adminAccount = service.settings.adminAccount;
                    if (adminAccount == null) {
                        TraceUtils.error(`[UserProvisioningService]: Cannot fetch user data from OAuth2 server for ${info.username}. The service cannot get authorized via the admin account.`);
                        return null;
                    }
                    // authorize user and get token
                    const authorizeUser = await service.authorize(adminAccount);
                    // get user profile
                    let OAuthUser = await service.getUser(info.username, authorizeUser);
                    if (OAuthUser == null) {
                        TraceUtils.error(`[UserProvisioningService]: An error occured while trying to fetch user data for ${info.username} from OAuth2 server.`);
                        return null;
                    }
                    // and assign data, if any
                    if (OAuthUser.familyName || OAuthUser.givenName) {
                        fullName = `${OAuthUser.familyName || ''} ${OAuthUser.givenName || ''}`;
                    }
                    /* if (OAuthUser.alternateName) {
                        email = OAuthUser.alternateName;
                    }*/
                    if (OAuthUser.hasOwnProperty('enabled')) {
                        userEnabled = OAuthUser.enabled ? -1 : 0;
                    }
                }
                // get unattended account from settings
                const unattendedAccount = context.getApplication().getConfiguration().getSourceAt('settings/auth/unattendedExecutionAccount');
                if (!unattendedAccount) {
                    TraceUtils.error(`[UserProvisioningService]: Cannot create guest user for ${info.username}. Unattended execution account must be configured.`);
                    return null;
                }
                // declare ownership to unattended account
                const owner = {
                    name: unattendedAccount
                };
                // create a user register action
                const userRegisterAction = {
                    name: info.username,
                    description: fullName,
                    email: email,
                    enabled: userEnabled,
                    groups: [
                        {
                            id: candidatesGroup
                        },
                    ],
                    owner: owner,
                    createdBy: owner,
                    modifiedBy: owner
                }
                // save user register action
                await context.model('UserRegisterAction').silent().save(userRegisterAction);
                // try to refresh user
                // note: if the above action is not auto completed, the user will not have instant access, which is ok
                const user = await context.model('User').where('name').equal(userRegisterAction.name).silent().getItem();
                // and return
                return user;
            }
            return null;
        } catch (err) {
            TraceUtils.error(`An error occurred while trying to validate user ${info.username} `);
            TraceUtils.error(err);
        }
    }
}
export {
    UserProvisioningService
};
