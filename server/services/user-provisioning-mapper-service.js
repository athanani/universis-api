import { ApplicationService, Guid, HttpForbiddenError, TraceUtils } from '@themost/common';
import { QueryExpression } from '@themost/query';
import { promisify } from 'util';

class UserMappingNotFoundError extends HttpForbiddenError {
    /**
     * @param {string=} msg 
     */
    constructor(msg) {
        super(msg || 'User provisioning mapper failed to map user context due to missing or inaccessible data');
        this.code = 'E_USER_MAPPING';
        this.statusCode = 403.11;
    }
}

class UserMappingViolationError extends HttpForbiddenError {
    /**
     * @param {string=} msg 
     */
    constructor(msg) {
        super(msg || 'User provisioning mapper failed to map user context due to a violation of the specified unique constraint');
        this.code = 'E_USER_MAPPING_VIOLATION';
        this.statusCode = 403.12;
    }
}

class UserMappingMissingAttributeError extends HttpForbiddenError {
    /**
     * @param {string=} msg 
     */
    constructor(msg) {
        super(msg || 'User provisioning mapper failed to map user context due to missing or inaccessible token attribute');
        this.code = 'E_USER_ATTRIBUTE_MISSING';
        this.statusCode = 403.13;
    }
}

class UserMappingEmptyAttributeError extends HttpForbiddenError {
    /**
     * @param {string=} msg 
     */
    constructor(msg) {
        super(msg || 'User provisioning mapper failed to map user context due to empty token attribute');
        this.code = 'E_USER_ATTRIBUTE_EMPTY';
        this.statusCode = 403.14;
    }
}

class UserProvisioningMapperService extends ApplicationService {
    /**
     * @param {DataContext} context
     * @param {{username:string,scope:string}} info 
     * @returns {Promise<{id:number,name:string,description:string}>}
     */
    async getUser(context, info) {
        return await context.model('User').where('name').equal(info.username).silent().getItem();
    }
}

class PersonAttributeMapperService extends UserProvisioningMapperService {

    /**
     * @type {Map<string,string>}
     */
    scopeMappings = new Map([
        [
            'students',
            'Students'
        ],
        [
            'teachers',
            'Instructors'
        ]
    ]);

    /**
     * @param {import('@themost/data').DataContext} context
     * @param {{username:string,scope:string}} info 
     * @returns {Promise<{id:number,name:string,description:string,required:boolean}>}
     */
    async getUser(context, info) {
        const scopes = info.scope.split(',');
        /**
         * @type {Array<{scope: string,userAttribute: string,tokenAttribute: string}>}
         */
        const userProvisioningConfiguration = context.getConfiguration().getSourceAt('settings/universis/userProvisioning') || [];
        const userProvisioningSettings = userProvisioningConfiguration.find((item) => {
            return scopes.includes(item.scope);
        });
        // if user provisioning settings have not been set
        if (userProvisioningSettings == null) {
            // use super method
            return super.getUser(context, info);
        }
        // if token does not have the specified attribute
        if (Object.prototype.hasOwnProperty.call(info, userProvisioningSettings.tokenAttribute) === false) {
            // if attribute is required based on user provisioning configuration
            if (userProvisioningSettings.required) {
                throw new UserMappingMissingAttributeError();
            }
            // otherwise continue
            return super.getUser(context, info); 
        }
        let ParentModel = null;
        // find target model
        this.scopeMappings.forEach((value, key) => {
            if (scopes.includes(key)) {
                ParentModel = value;
            }
        });
        // if target model is null
        if (ParentModel == null) {
            // use super method
            return super.getUser(context, info); 
        }
        const value = info[userProvisioningSettings.tokenAttribute];
        if (value == null) {
            // if attribute is required based on user provisioning configuration
            if (userProvisioningSettings.required) {
                // throw error
                throw new UserMappingEmptyAttributeError();
            }
            // otherwise continue
            return super.getUser(context, info);
        }
        const results = await context.model(ParentModel)
            .where(userProvisioningSettings.userAttribute).equal(value)
            .select('id', 'user/id as user', 'user/name as username').take(1).silent().getList();
        if (results.total === 0) {
            TraceUtils.error('UserProvisioning', `User provisioning mapper did not find a user after searching for [${ParentModel}].[${userProvisioningSettings.userAttribute}] equal to "${value}".`)
            throw new UserMappingNotFoundError();
        }
        if (results.total > 1) {
            TraceUtils.error('UserProvisioning', `User provisioning mapper found more than one user after searching for [${ParentModel}].[${userProvisioningSettings.userAttribute}] equal to "${value}".`)
            throw new UserMappingViolationError();
        }
        /**
         * @type {{id:number,user: number,username: string}}
         */
        const result = results.value[0];
        if (result && result.username != null && result.username !== info.username) {
            TraceUtils.log('UserProvisioning', `User provisioning mapper is going to reset user with name "${result.username}" to "${info.username}" due to a conflict occured after searching for [${ParentModel}].[${userProvisioningSettings.userAttribute}] with value equal to "${value}".`)
            /**
             * @type {function(query):Promise<*>}
             */
            const executeAsync = promisify(context.db.execute.bind(context.db));
            await new Promise((resolve, reject) => {
                if (result.user == null) {
                    return resolve();
                }
                return context.db.executeInTransaction((cb) => {
                    // 1/ try to update user who has the username of the returned user
                    const UserReferences = context.model('UserReference').sourceAdapter;
                    return executeAsync(
                        new QueryExpression().update(UserReferences).set({
                            name: Guid.newGuid().toString()
                        }).where('name').equal(info.username), null
                    ).then(() => {
                        // 2/ try to set username
                        return executeAsync(
                            new QueryExpression().update(UserReferences).set({
                                name: info.username
                            }).where('id').equal(result.user), null
                        ).then(() => {
                            TraceUtils.log('UserProvisioning', `User provisioning mapper is finishing resetting user with name "${result.username}" to "${info.username}".`)
                            return cb();
                        });
                    }).catch((err) => {
                        return cb(err);
                    });
                }, (err) => {
                    if (err) {
                        return reject(err);
                    }
                    return resolve();
                });
            });
        }
        return super.getUser(context, info);
    }
}

export {
    UserMappingNotFoundError,
    UserMappingViolationError,
    UserMappingMissingAttributeError,
    UserMappingEmptyAttributeError,
    UserProvisioningMapperService,
    PersonAttributeMapperService
}