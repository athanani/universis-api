import {AttachmentFileSystemStorage} from '@themost/web'
import path from 'path';
import util from 'util';
import {serviceRouter} from '@themost/express';
import {TraceUtils, HttpNotFoundError, DataNotFoundError, HttpConflictError} from '@themost/common';
import {DefaultScopeAccessConfiguration, ScopeAccessConfiguration} from '@universis/janitor';
// noinspection JSUnusedGlobalSymbols
/**
 * @class
 */
export class SharedContentService extends AttachmentFileSystemStorage {
    /**
     * @param {IApplication} app
     */
    constructor(app) {
        // get physical path from application configuration or use content/shared as default path
        super(path.resolve( app.getConfiguration().getSourceAt('settings/universis/content/shared') || 'content/shared'));
        // set virtual path
        // noinspection JSUnusedGlobalSymbols
        this.virtualPath = '/api/content/shared/';
        // add route
        serviceRouter.get('/content/shared/:file', async function getSharedContent(req, res, next) {
            // get shared content service instance
            let service = req.context.getApplication().getService(SharedContentService);
            // validate service
            if (service == null) {
                throw new Error('Shared content service cannot be found or is in accessible');
            }
            const findOne = util.promisify(service.findOne).bind(service);
            // find item by alternate name
            const item = await findOne(req.context, { alternateName: req.params.file });
            // throw not found if empty
            if (item == null) {
                return next(new HttpNotFoundError());
            }
            // validate url
            const itemURL = item.url.replace(/(\\+)/g,'/');
            // if item URL is relative to service virtual path do nothing
            if (new RegExp('^' + service.virtualPath, 'g').test(itemURL) === false) {
                // otherwise throw bad request exception
                return next(new HttpConflictError());
            }
            const resolvePhysicalPath = util.promisify(service.resolvePhysicalPath).bind(service);
            const sendFile = util.promisify(res.sendFile).bind(res);
            try {
                 //get physical file path
                 const physicalPath = await resolvePhysicalPath(req.context, item);
                 // send file
                 await sendFile(physicalPath);
            }
            catch (err) {
                // if resolved file cannot be found in the specified path
                if (err.code === 'ENOENT') {
                    TraceUtils.error(`An error occurred while trying to resolve physical path for ${item.name}.`);
                    TraceUtils.error(err);
                    // throw not found error
                    return next(new HttpNotFoundError());
                }
                // otherwise throw error
                return next(err);
            }
        });
        TraceUtils.info('Services: SharedContentService service has been successfully installed');
        TraceUtils.info(`Services: SharedContentService service starts using "${this.root}" as content root.`);
        // auto update scope access
        let scopeAccessConfiguration = app.getConfiguration().getStrategy(ScopeAccessConfiguration);
        if (scopeAccessConfiguration == null) {
            // register ScopeAccessConfiguration
            app.getConfiguration().useStrategy(ScopeAccessConfiguration, DefaultScopeAccessConfiguration);
            // get service
            scopeAccessConfiguration = app.getConfiguration().getStrategy(ScopeAccessConfiguration);
        }
        // try to find item in scope access configuration
        const findElement = scopeAccessConfiguration.elements.find( x => {
            return x.resource === '/api/content/shared/(\\w+)'
        });
        // if item cannot be found
        if (findElement == null) {
            // add default read access  for teachers and students
            scopeAccessConfiguration.elements.push({
                "scope": [
                    "teachers",
                    "students"
                ],
                "resource": "/api/content/shared/(\\w+)",
                "access": [
                    "read"
                ]
            });
            TraceUtils.info(`Services: ScopeAccessConfiguration service has been successfully updated to include default read access for shared content.`);
        }
    }

    resolvePhysicalPath(context, item, callback) {
        // override super.resolvePhysicalPath()
        const self = this;
        if (item.alternateName) {
            return callback(null, path.resolve(self.root, './', item.alternateName));
        }
        return this.findOne(context, item, function(err, result) {
            if (err) {
                return callback(err);
            }
            if (result == null) {
                return new DataNotFoundError('The specified content item cannot be found');
            }
            return callback(null, path.resolve(self.root, './', item.alternateName));
        });
    }

    copyFrom(context, src, attrs, callback) {
        return callback(new Error('Shared content is read-only. Write operation is not supported. Use other content services to complete this operation.'));
    }

    remove(context, item, callback) {
        return callback(new Error('Shared content is read-only. Remove operation is not supported. Use other content services to complete this operation.'));
    }

}
