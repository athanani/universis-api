// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication} from '@themost/express';
import {ApplicationService, TraceUtils} from "@themost/common";
import cluster from 'cluster';
import {writeAccessLog} from '../log';

const DATABASE_HEALTH_STATUS_URL = '/api/system/diagnostics/database/health';

// noinspection JSUnusedLocalSymbols
/**
 * Calculates the difference in milliseconds of two HRTime parameters
 * @param {HRTime} startAt
 * @param {HRTime} endAt
 * @return {number}
 */
// eslint-disable-next-line no-unused-vars
function getMilliseconds(startAt, endAt) {
    return (((endAt[0] * 1e9) + endAt[1]) / 1e6) - (((startAt[0] * 1e9) + startAt[1]) / 1e6)
}
// noinspection JSUnusedGlobalSymbols
class DatabaseHealthService extends ApplicationService {
    /**
     * @param {ExpressDataApplication|*} app
     */
    constructor(app) {
        super(app);
        // run on in master
        if (cluster.isMaster) {
            // add timer
            // try to get interval from configuration
            let interval = app.getConfiguration().getSourceAt('settings/universis/databaseHealth/interval');
            if (typeof interval === 'number') {
                if (interval <= 0) {
                    TraceUtils.info('Services: DatabaseHealthService is being disabled due to configuration settings.');
                    return;
                }
            }
            else {
                interval = 60000;
            }
            TraceUtils.info(`Services: DatabaseHealthService is starting. Health status interval has been set to ${interval}ms.`);
            this.queryStatus();
            setInterval(()=> {
                this.queryStatus();
            }, interval);
        }
    }

    queryStatus() {
        // create context
        const context = this.getApplication().createContext();
        // do something very simple
        const startAt = process.hrtime();
        context.model('Workspace').asQueryable().getItem().then( () => {
            // get end time
            const endAt = process.hrtime();
            // write access log (simulate request)
            writeAccessLog({
                // set remote address to default
                connection: {
                    remoteAddress: '0.0.0.0'
                },
                // set context user as system
                context: {
                    user: {
                        name: "system"
                    }
                },
                method: 'GET',
                headers: { },
                url: DATABASE_HEALTH_STATUS_URL,
                originalUrl: DATABASE_HEALTH_STATUS_URL,
                httpVersion: '1.1',
                httpVersionMajor: '1',
                httpVersionMinor: '1'
            }, {
                // set status to 200
                statusCode: 200
            }, startAt, endAt);
            return context.finalize();
        }).catch( err => {
            TraceUtils.error('An error occurred while trying to query database health.');
            TraceUtils.error(err);
            // get end time
            const endAt = process.hrtime();
            writeAccessLog({
                connection: {
                    remoteAddress: '0.0.0.0'
                },
                context: {
                    user: {
                        name: "system"
                    }
                },
                method: 'GET',
                headers: { },
                url: DATABASE_HEALTH_STATUS_URL,
                originalUrl: DATABASE_HEALTH_STATUS_URL,
                httpVersion: '1.1',
                httpVersionMajor: '1',
                httpVersionMinor: '1'
            }, {
                // set status to 500
                statusCode: 500
            }, startAt, endAt);
            if (context) {
                return context.finalize();
            }
        });
    }

}

export {DatabaseHealthService};
