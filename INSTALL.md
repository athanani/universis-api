# UniverSIS-api Installation

* Checkout the source code by executing the following command

  `git clone https://gitlab.com/universis/universis-api.git`

* Go to project directory:

  `cd universis-api`

Continue with the installation instructions, or jump to [Docker installation](INSTALL.md#docker-installation)

## Prerequisites

**1. Install node.js**

Node.js version > 6.14.3 is required. Visit [Node.js](https://nodejs.org/en/) and follow the installation instructions provided for your operating system.

**2. Install Ngnix or apache web server (Optional)**

A frontend web server may be installed and used as a proxy server for any node.js application which is a part of universis project.
This web server may be the [nginx web server](https://nginx.org/en/), the [Apache HTTP server](https://httpd.apache.org/) or any other server application which may operate as proxy server.

**_Nginx web server installation_**

[How to install Nginx on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-16-04)

[How to install Nginx on CentOS 7](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-centos-7)

[How to install Nginx on Windows](https://vexxhost.com/resources/tutorials/nginx-windows-how-to-install/)


**_Apache web server 2.4 installation_**

[How to install the Apache Web Server on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-ubuntu-16-04)

[How to install Apache Web Server on CentOS 7](https://www.linode.com/docs/web-servers/apache/install-and-configure-apache-on-centos-7/)

[How to install Apache Web Server on Windows](https://httpd.apache.org/docs/2.4/platform/windows.html)

**3. Install PM2 Process Manager (Optional)**

[PM2 Advanced Process Manager for node.js](http://pm2.keymetrics.io/) may be installed by following the installation instructions
provided in [PM2 quick start article](http://pm2.keymetrics.io/docs/usage/quick-start/)

    npm install pm2@latest -g

[How to install pm2 as service on Linux Distributions](http://pm2.keymetrics.io/docs/usage/startup/)

[How to install pm2 as service on Windows](https://www.npmjs.com/package/pm2-windows-service)

## Install dependencies

Install application development dependencies by executing the following command:

`npm i`

## Configure application

Application configuration base settings are defined in server/config/app.json file.
Copy server/config/app.json to **server/config/app.production.json**

`cp server/config/app.json server/config/app.production.json`

or on Windows

`copy server\config\app.json server\config\app.production.json`

Edit the configuration file accordingly. The configuration parameters are explained in our [configuration wiki page](https://gitlab.com/universis/universis-api/wikis/Configuration)

## Deploy

1. Build from source code

  Build source code by executing the following command:

  `npm run build`

2. Register PM2 Process

  Use the configuration file for pm2.config.json

    {
      "name"        : "universis_api",
      "script"      : "./bin/www",
      "watch"       : false,
      "env": {
        "IP": "0.0.0.0"
      }
    }
  
  Register PM2 process by executing:

  `pm2 start pm2.config.json`
  
  ##### Enable cluster mode
  
  If you want to enable application cluster mode read [PM2 Cluster Mode](http://pm2.keymetrics.io/docs/usage/cluster-mode/) instructions. 
  Here is an example of pm2.config.json with cluster mode enabled:
  
    {
        "name"        : "universis_api",
        "script"      : "./bin/www",
        "watch"       : false,
        "env": {
          "IP": "0.0.0.0"
        },
        "instances" : "max",
        "exec_mode": "cluster"
    }

 ##### Enable development mode
  
  If you want to enable application development mode use the following pm2.config.json:
  
    {
        "name"        : "universis_api",
        "script"      : "./bin/www",
        "watch"       : false,
        "env": {
          "IP": "0.0.0.0",
          "NODE_ENV": "development"
        },
        "node_args": [
            "--require",
            "@babel/register"
        ]
    }
    
 Remember that when application runs in development mode uses server/config/app.development.json configuration file. So you should create or edit 
 this file to follow your development environment.    

3. Navigate to http://localhost:5001

# Docker installation

1. Navigate to application root directory and build docker image:

  `docker build . -t universis-api:latest`

2. Create docker container

    ```
    docker run -d \
      --name=api \
      --mount source=api,destination=/usr/src/app/server/config \
      universis-api:latest
    ```

3. Edit `src/config/app.production.json` in docker mount volume `/var/lib/docker/volumes/api/_data/`
and set client_id and client_secret of OAuth2 Server Application

    ```
    "settings": {
    ...
        "auth": {
            ...
            "client_id":".....",
            "client_secret":"...."
        },
    ...
    }
    ```

    The configuration parameters are explained in our [configuration wiki page](https://gitlab.com/universis/universis-api/wikis/Configuration)

4. Restart container

  `docker restart api`
